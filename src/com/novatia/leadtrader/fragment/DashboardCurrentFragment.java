package com.novatia.leadtrader.fragment;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.activity.StockItemActivity;
import com.novatia.leadtrader.adapters.CurrentStockAdapter;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.Stock;


import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DashboardCurrentFragment extends ListFragment{
	ListView currentStockList;
	
	ArrayList<Stock> portfolio = new ArrayList<Stock>();
	
	LCService mService;
	boolean mBound;
	
	CurrentStockAdapter adapter;
	
	Handler h;
	
	ProgressDialog dialog;
	
	private ServiceConnection mConnection = new ServiceConnection() 
	{

	    @Override
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        // We've bound to LocalService, cast the IBinder and get LocalService instance
	        LCService.MyBinder binder = (LCService.MyBinder) service;
	        mService = binder.getService();
	        mBound = true;
	        displayUserPorfolio();
//	        Toast.makeText(getActivity(), "Conncted", Toast.LENGTH_LONG)
//	          .show();
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName arg0) {
	        mBound = false;
//	        Toast.makeText(getActivity(), "Not Conncted", Toast.LENGTH_LONG)
//	          .show();
	        
	    }
	};
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.fragment_dashboard_current, container, false);
		
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	
		
		
		portfolio = new ArrayList<Stock>();
		adapter = new CurrentStockAdapter(getActivity(), portfolio);
		setListAdapter(adapter);
		
		
		
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);
		
	}
	
	@Override
	public void onPause(){
		if(mBound){
			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}
	
	
	@Override  
	  public void onListItemClick(ListView l, View v, int position, long id) {  
		
	    Intent i = new Intent(getActivity(), StockItemActivity.class);
	    i.putExtra("ref", portfolio.get(position).getInstrumentId());
	    i.putExtra("price", portfolio.get(position).getPrice());
	    startActivity(i);
//		Toast.makeText(getActivity().getApplicationContext(), "It works", 
//				   Toast.LENGTH_LONG).show();
	  }
	
	public void displayUserPorfolio(){
		h = new Handler();
		dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mService.setUserPortfolio();
				
				h.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						portfolio = mService.getCurrentUser().getPorfolio();
//						Toast.makeText(getActivity(), Integer.toString(portfolio.size()), Toast.LENGTH_LONG)
//				          .show();
						adapter = new CurrentStockAdapter(getActivity(), portfolio);
						setListAdapter(adapter);
						adapter.notifyDataSetChanged();
						dialog.dismiss();
						
					}
					
				});
				
				
			}
			
		});
		thread.start();
	}

}
