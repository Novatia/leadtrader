package com.novatia.leadtrader.fragment;

import com.novatia.leadtrader.activity.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class PasswordRequestDialog extends DialogFragment{
	EditText userIdText;
	Button requestButton;
	
	public PasswordRequestDialog(){
		
	}
	
	public static PasswordRequestDialog newInstance(){
		
		PasswordRequestDialog dialog = new PasswordRequestDialog();
//		Bundle args = new Bundle();
//		args.putString("title", title);
//		dialog.setArguments(args);
		
		return dialog;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.fragment_forgot_password, container);
		userIdText = (EditText)view.findViewById(R.id.userIdText);
		requestButton = (Button)view.findViewById(R.id.sendPasswordRequest);
		
//		String title = getArguments().getString("title");
//		getDialog().setTitle(title);
		
		userIdText.requestFocus();
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		return view;
		
	}
	
	

}
