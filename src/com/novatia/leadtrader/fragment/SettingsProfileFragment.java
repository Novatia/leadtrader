package com.novatia.leadtrader.fragment;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.novatia.lc.service.LCService;
import com.novatia.lc.types.User;
import com.novatia.leadtrader.activity.R;

public class SettingsProfileFragment extends Fragment{
	
	EditText accountName, username, accountId;
	View view;
	
	LCService mService;
	boolean mBound;
	
	Handler handler;
	
	private ServiceConnection mConnection = new ServiceConnection() 
	{

	    @Override
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        // We've bound to LocalService, cast the IBinder and get LocalService instance
	        LCService.MyBinder binder = (LCService.MyBinder) service;
	        mService = binder.getService();
	        mBound = true;
	        displayUserProfile();
	        
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName arg0) {
	        mBound = false;

	        
	    }
	};
	
	
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
      // Defines the xml file for the fragment
      view = inflater.inflate(R.layout.fragment_settings_profile , container, false);
      // Setup handles to view objects here
      // etFoo = (EditText) v.findViewById(R.id.etFoo);
      
      accountName = (EditText)view.findViewById(R.id.accountName);
      accountId = (EditText)view.findViewById(R.id.accountId);
      username = (EditText)view.findViewById(R.id.username);
      
      
      return view;
    }
	
	
//	public void onCreate(Bundle savedInstanceState){
//		super.onCreate(savedInstanceState);
//		
//		accountName = (EditText)getView().findViewById(R.id.accountName);
//		accountId = (EditText)getView().findViewById(R.id.accountId);
//		username = (EditText)getView().findViewById(R.id.username);
//		
//	}
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);
		
	}
	
	@Override
	public void onPause(){
		if(mService != null){
//			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}
	
	
	public void displayUserProfile(){
		
		handler = new Handler();
		
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				final User user = mService.getCurrentUser();
				
				handler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						accountName.setText(user.getAccountName());
						accountId.setText(user.getAccountId());
						username.setText(user.getUserId());
						
					}
					
				});
				
			}
			
		});
		
		thread.start();
	}

}
