package com.novatia.leadtrader.fragment;

import com.novatia.lc.helper.DecimalPlaces;
import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.Side;
import com.novatia.leadtrader.activity.R;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class NewOrderFragment extends DialogFragment implements OnClickListener{

	RadioButton buySide;
	RadioButton sellSide;
	
	RadioButton marketType;
	RadioButton limitType;
	
	EditText instrumentIdEditText;
	EditText instrumentPriceEditText;
	EditText quantityEditText;
	EditText valueEditText;
	EditText limitPriceEditText;
	
	Button addOrderButton;
	
	String condition = "M";
	String side;
	
	RadioGroup sideType;
	RadioGroup priceType;
	
	Handler handler;
	
	
	LCService mService;
	boolean mBound;
	
//	boolean state = false;
	
	ProgressDialog dialog;

public NewOrderFragment(){
		super();
	
	
}	
	
	
private ServiceConnection mConnection = new ServiceConnection() 
{

    @Override
    public void onServiceConnected(ComponentName className,
            IBinder service) {
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        LCService.MyBinder binder = (LCService.MyBinder) service;
        mService = binder.getService();
        mBound = true;

    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        mBound = false;

        
    }
};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.new_order, container);
		
		
		getDialog().setTitle("Add Order");
		
		buySide = (RadioButton)view.findViewById(R.id.buySide);
		sellSide = (RadioButton)view.findViewById(R.id.sellSide);
		
		marketType = (RadioButton)view.findViewById(R.id.marketType);
		limitType = (RadioButton)view.findViewById(R.id.limitType);
		
		instrumentIdEditText = (EditText)view.findViewById(R.id.instrumentIdEditText);
		instrumentPriceEditText = (EditText)view.findViewById(R.id.instrunmentPriceEditText);
		quantityEditText = (EditText)view.findViewById(R.id.quantityEditText);
		valueEditText = (EditText)view.findViewById(R.id.valueEditText);
		limitPriceEditText = (EditText)view.findViewById(R.id.limitPriceEditText);
		
		addOrderButton = (Button)view.findViewById(R.id.orderButton);
		addOrderButton.setOnClickListener(this);
		
		sideType = (RadioGroup)view.findViewById(R.id.sideType);
		priceType = (RadioGroup)view.findViewById(R.id.priceType);
		
		
		
		
		Bundle bundle = getArguments();
		String price = bundle.getString("price");
		String ref = bundle.getString("ref");
		String side = bundle.getString("side");
		
		setSelectedInstrumentDetails(ref, price);
		checkSide(side);
		
		
		quantityEditText.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				String q = quantityEditText.getText().toString();
				
				if(s.length() > 0){
				
				int quantity = Integer.parseInt(q);
				
				double price = Double.parseDouble(instrumentPriceEditText.getText().toString());
				
				double value = price * quantity;
				
				value = DecimalPlaces.round(value, 2);
				
				valueEditText.setText("N"+Double.toString(value));
				}
				
			}
			
		});
		
		
		priceType.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(RadioGroup arg0, int checkedId) {
				// TODO Auto-generated method stub
				condition = checkedId == R.id.marketType ? "M" : "L";
				
				if(checkedId == R.id.limitType){
					limitPriceEditText.setEnabled(true);
				}else{
					limitPriceEditText.setEnabled(false);
				}
				
			}
			
		});
		
		sideType.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(RadioGroup arg0, int checkedId) {
				// TODO Auto-generated method stub
				condition = checkedId == R.id.buySide ? "B" : "S";
				
			}
			
		});
		
		
		
		
		return view;
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);
		
		
		
	}
	
	@Override
	public void onPause(){
		if(mBound){
			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}
	
	public void setSelectedInstrumentDetails(String ref, String price){
		instrumentIdEditText.setText(ref);
		instrumentPriceEditText.setText(price);
	}

	public void checkSide(String side){
		if(side.equals("buy")){
			side = Side.buy;
			buySide.setChecked(true);
			
		}else if(side.equals("sell")){
			side = Side.sell;
			sellSide.setChecked(true);
		}else{
			
		}
	}
	
	public boolean isQuantityEntered(){
		String quan = quantityEditText.getText().toString();
		if(!quan.equals("") && !quan.equals("0")){
			return true;
		}
		return false;
	}
	
	public boolean isSideChecked(){
		if(buySide.isChecked() || sellSide.isChecked()){
			
			side = buySide.isChecked() ? Side.buy : Side.sell;
			
			
			return true;
		}
		
		return false;
	}



	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		if(isSideChecked()){
			if(limitType.isChecked()){
				String limitPriceValue = limitPriceEditText.getText().toString();
				if(limitPriceValue.equals("")){
					Toast.makeText(getActivity(), "Enter limit price.", Toast.LENGTH_LONG)
  		          .show();
				}else if(limitPriceValue.equals("0")){
					
					Toast.makeText(getActivity(), "Limit price must be greater than 0.", Toast.LENGTH_LONG)
	  		          .show();
					
				}else if(limitPriceValue.equals(" ")){
//					dialog.dismiss();
					Toast.makeText(getActivity(), "Enter limit price.", Toast.LENGTH_LONG)
	  		          .show();
				}else{
					if(InternetHelper.isOnline()){
						dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
					processOrder();
					}else{
						
						dismiss();
						Toast.makeText(getActivity(), "Check your connection.", Toast.LENGTH_LONG)
		  		          .show();
						
					}
				}
			}
			else{
				dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
				processOrder();
				
			}
			
		}else{
			
//			TODO toast to inform that you need to select side
			Toast.makeText(getActivity(), "Select Buy or Sell.", Toast.LENGTH_LONG)
	          .show();
		}
		
		
		
	}
	
	
	public void processOrder(){
		handler = new Handler();
		
//		dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
		
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				final double total_amount = Double.parseDouble(valueEditText.getText().toString().substring(1, valueEditText.getText().toString().length()));
				
				final boolean result = mService.addNewOrder(instrumentIdEditText.getText().toString(), quantityEditText.getText().toString(), instrumentPriceEditText.getText().toString(), limitPriceEditText.getText().toString(), side, condition);
				
				final boolean is_more_than_cash = mService.isLessThanOrEqualCashBalance(total_amount);
				
				handler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						dialog.dismiss();
						
						if(!is_more_than_cash){
//							dialog.dismiss();
							dismiss();
							Toast.makeText(getActivity(), "Not enough cash balance", Toast.LENGTH_LONG);
							return;
						}
						
						else if (result){
//							dialog.dismiss();
							dismiss();
							Toast.makeText(getActivity(), "Order successful.", Toast.LENGTH_LONG)
			  		          .show();
							
						}else{
							dismiss();
							Toast.makeText(getActivity(), "Order failed try again later.", Toast.LENGTH_LONG)
			  		          .show();
							
							
						}
						
					}
					
				});
				
			}
			
		});
		
		thread.start();
		
	}
	

}
