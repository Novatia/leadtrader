package com.novatia.leadtrader.fragment;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.adapters.OrdersAdapter;
import com.novatia.lc.helper.DateHelper;
import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.MyDate;
import com.novatia.lc.types.Order;

public class DashboardHistoricFragment extends ListFragment implements OnClickListener{
	
	LCService mService;
	boolean mBound;
	
	OrdersAdapter adapter;
	ArrayList<Order> orders;
	
	Button fromButton;
	Button toButton;
	
	MyDate fromDate = new MyDate();
	MyDate toDate = new MyDate();
	
	Handler h = new Handler();
	
	ProgressDialog dialog;
	
	String finalStartDate;
	String finalEndDate;
	
	private ServiceConnection mConnection = new ServiceConnection() 
	{

	    @Override
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        // We've bound to LocalService, cast the IBinder and get LocalService instance
	        LCService.MyBinder binder = (LCService.MyBinder) service;
	        mService = binder.getService();
	        mBound = true;
	        
	        setOrders(DateHelper.someDaysAgo(-100), DateHelper.someDaysAgo(1));
	        
//	        Toast.makeText(getActivity(), DateHelper.someDaysAgo(-100), Toast.LENGTH_LONG)
//	          .show();
//	        Toast.makeText(getActivity(), "started", Toast.LENGTH_LONG)
//	          .show();
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName arg0) {
	        mBound = false;
//	        Toast.makeText(getActivity(), "Not Conncted", Toast.LENGTH_LONG)
//	          .show();
	        
	    }
	};
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.fragment_dashboard_historic, container, false);
		
		fromButton = (Button)view.findViewById(R.id.fromDateButton);
		toButton = (Button)view.findViewById(R.id.toDateButton);
		
		fromButton.setOnClickListener(this);
		toButton.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);	
		orders = new ArrayList<Order>();
		adapter = new OrdersAdapter(getActivity(), orders);
		setListAdapter(adapter);
		
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);
		
		
		
	}
	
	@Override
	public void onPause(){
		if(mBound){
			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}
	
	
	

	@Override
	public void onClick(View arg0) {
		
		
		
		final Calendar calendarRange = Calendar.getInstance();
        final int maxYear = calendarRange.get(Calendar.YEAR);
        final int maxMonth = calendarRange.get(Calendar.MONTH);
        final int maxDay = calendarRange.get(Calendar.DAY_OF_MONTH);
		
		// TODO Auto-generated method stub
		if(arg0.getId() == R.id.fromDateButton){
			DatePickerDialog fromDateDialog = new DatePickerDialog(getActivity(), new mFromDateSetListener(),
					maxYear, maxMonth, maxDay);
			fromDateDialog.show();
			
			
		}
		
		if(arg0.getId() == R.id.toDateButton){
//			DatePickerDialog toDateDialog = new DatePickerDialog();
			DatePickerDialog toDateDialog = new DatePickerDialog(getActivity(), new mToDateSetListener(),
					maxYear, maxMonth, maxDay);
			toDateDialog.show();
		    
		    
		}
		
	}
	
	
	public void setOrders(String startDate, String endDate){
		
		finalStartDate = startDate;
		finalEndDate = endDate;
			
		dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
		
			Thread thread = new Thread(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					final ArrayList<Order> nOrders = mService.getUserOrders(finalStartDate, finalEndDate);
					h.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							dialog.dismiss();
							orders = nOrders;
							adapter = new OrdersAdapter(getActivity(), orders);
							setListAdapter(adapter);
							adapter.notifyDataSetChanged();
							
						}
						
					});
				}
	    		
	    	});
		
		thread.start();
		
		
        	
	}
	
	class mFromDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            // TODO Auto-generated method stub
            // getCalender();
            fromDate.setYear(Integer.toString(year));
            fromDate.setMonth(Integer.toString(monthOfYear));
            fromDate.setDay(Integer.toString(dayOfMonth));
            
//            Toast.makeText(getActivity(), fromDate.toString(), Toast.LENGTH_LONG)
//	          .show();

        }
    }
	
	class mToDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            if(InternetHelper.isOnline()){
            	if(fromDate != null){
                	
                	// TODO Auto-generated method stub
                    // getCalender();
                    int mYear = year;
                    int mMonth = monthOfYear;
                    int mDay = dayOfMonth;
                    
                    toDate.setYear(Integer.toString(year));
                    toDate.setMonth(Integer.toString(monthOfYear));
                    toDate.setDay(Integer.toString(dayOfMonth));
                    
                    if(fromDate.getDateObj().compareTo(toDate.getDateObj()) < 0){
                    	
                    	setOrders(DateHelper.specificDate(fromDate.date()), DateHelper.specificDate(toDate.date()));
                    	
                    }else{
                    	Toast.makeText(getActivity(), "Please select date again.", Toast.LENGTH_LONG)
             	          .show();
                    	
                    	
                    }
                    
                    
                    
//                    Toast.makeText(getActivity(), toDate.date(), Toast.LENGTH_LONG)
//        	          .show();
                	
                }else{
                	
                	 Toast.makeText(getActivity(), "Please select from date first.", Toast.LENGTH_LONG)
       	          .show();
                }
            	
            	
            }else{
            	Toast.makeText(getActivity(), "Check your connection.", Toast.LENGTH_LONG)
     	          .show();
            	
            }
            


        }
    }
	

}
