package com.novatia.leadtrader.fragment;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.leadtrader.activity.R;

public class SettingsPasswordFragment extends Fragment implements OnClickListener{
	
	LCService mService;
	boolean mBound;
	
	Handler handler;
	
	EditText currentPassword, newPassword, confirmPassword;
	
	Button changePassword;
	
	ProgressDialog dialog;
	
	private ServiceConnection mConnection = new ServiceConnection() 
	{

	    @Override
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        // We've bound to LocalService, cast the IBinder and get LocalService instance
	        LCService.MyBinder binder = (LCService.MyBinder) service;
	        mService = binder.getService();
	        mBound = true;
	        
	        
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName arg0) {
	        mBound = false;

	        
	    }
	};
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
      // Defines the xml file for the fragment
      View view = inflater.inflate(R.layout.fragment_settings_password , container, false);
      
      currentPassword = (EditText)view.findViewById(R.id.oldPasswordEditText);
      newPassword = (EditText)view.findViewById(R.id.newPasswordEditText);
      confirmPassword = (EditText)view.findViewById(R.id.confirmPasswordEditText);
      changePassword = (Button)view.findViewById(R.id.changePasswordButton);
      
      changePassword.setOnClickListener(this);
      
      return view;
    }
	
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);
		
	}
	
	@Override
	public void onPause(){
		if(mService != null){
//			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
		if(InternetHelper.isOnline()){
			if(passwordFieldsNotEmpty()){
				if(newPassword.getText().toString().equals(confirmPassword.getText().toString())){
					changePassword();
					
				}else{
					dialog.dismiss();
					Toast.makeText(getActivity(), "New password and confirm password must be the same.", Toast.LENGTH_LONG)
			          .show();
					
				}
				
				
			}else{
				dialog.dismiss();
				Toast.makeText(getActivity(), "Fields must be filled.", Toast.LENGTH_LONG)
		          .show();
			}
		}else{
			dialog.dismiss();
			Toast.makeText(getActivity(), "Check your Internet", Toast.LENGTH_LONG)
	          .show();
		}
		
	}
	
	public boolean passwordFieldsNotEmpty(){
		if(currentPassword.getText().toString().equals("") || newPassword.getText().toString().equals("") || confirmPassword.getText().toString().equals("") || currentPassword.getText().toString().equals(" ") || newPassword.getText().toString().equals(" ") || confirmPassword.getText().toString().equals(""))
			return false;
		return true;
					
	}
	
	public void changePassword(){
		
		handler = new Handler();
		
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(mService.getCurrentUser().getPassword().equals(currentPassword.getText().toString())){
					final boolean state = mService.changeUserPassword(currentPassword.getText().toString(), newPassword.getText().toString());
					handler.post(new Runnable(){

						@Override
						public void run() {
							dialog.dismiss();
							// TODO Auto-generated method stub
							if(state)
								Toast.makeText(getActivity(), "Password changed successfully.", Toast.LENGTH_LONG)
								.show();
							else
								Toast.makeText(getActivity(), "Something went wrong try again later.", Toast.LENGTH_LONG)
								.show();
						}
						
					});
					
					
					
				}else{
					handler.post(new Runnable(){

						@Override
						public void run() {
							dialog.dismiss();
							// TODO Auto-generated method stub
							Toast.makeText(getActivity(), "Incorrect current password.", Toast.LENGTH_LONG)
					          .show();
						}
						
					});
					
				}
			}
			
		});
		
		thread.start();
	}
}
