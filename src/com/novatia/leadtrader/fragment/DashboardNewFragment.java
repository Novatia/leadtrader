package com.novatia.leadtrader.fragment;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.adapters.OrdersAdapter;
import com.novatia.lc.types.Order;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DashboardNewFragment extends ListFragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.fragment_dashboard_new, container, false);
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	
		
		
		ArrayList<Order> orders = new ArrayList<Order>();
		OrdersAdapter adapter = new OrdersAdapter(getActivity(), orders);
		setListAdapter(adapter);
	}

}
