package com.novatia.leadtrader.fragment;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.activity.StockItemActivity;
import com.novatia.leadtrader.adapters.NSEAdapter;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.Stock;

public class DashboardNSEFragment extends ListFragment{
	
	LCService mService;
	boolean mBound;
	
	Handler h;
	
	NSEAdapter adapter;
	
	ArrayList<Stock> orders;
	
	ProgressDialog dialog;
	
	EditText search;
	
	View view;
	
	
	
	private ServiceConnection mConnection = new ServiceConnection() 
	{

	    @Override
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        // We've bound to LocalService, cast the IBinder and get LocalService instance
	        LCService.MyBinder binder = (LCService.MyBinder) service;
	        mService = binder.getService();
	        mBound = true;
	        loadNSEData();
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName arg0) {
	        mBound = false;
//	        Toast.makeText(getActivity(), "Not Conncted", Toast.LENGTH_LONG)
//	          .show();
	        
	    }
	};
	
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		view = inflater.inflate(R.layout.fragment_dashboard_nse, container, false);
		
		search = (EditText)view.findViewById(R.id.search);
		search.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
//				Toast.makeText(getActivity(), "working", Toast.LENGTH_LONG);
				Log.i("searching....", "text changed working");
				adapter.getFilter().filter(search.getText().toString());
				adapter.notifyDataSetChanged();
				
			}
			
		});
		
		
		return view;
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);	
		orders = new ArrayList<Stock>();
		adapter = new NSEAdapter(getActivity(), orders);
		
		setListAdapter(adapter);
		
		
		
		
		
	}
	
	@Override  
	  public void onListItemClick(ListView l, View v, int position, long id) {  
	    Intent i = new Intent(getActivity(), StockItemActivity.class);
	    i.putExtra("ref", adapter.getItem(position).getInstrumentId());
	    i.putExtra("price", adapter.getItem(position).getPrice());
	    startActivity(i);
//		Toast.makeText(getActivity().getApplicationContext(), "It works", 
//				   Toast.LENGTH_LONG).show();
	  } 
	
	@Override
	public void onResume(){
		super.onResume();
		Intent intent= new Intent(getActivity(), LCService.class);
		getActivity().bindService(intent, mConnection,
		        Context.BIND_AUTO_CREATE);		
		
	}
	
	@Override
	public void onPause(){
		if(mBound){
			getActivity().unbindService(mConnection);
		}
		super.onPause();
		
	}
	
	public void loadNSEData(){
		h = new Handler();
		dialog = ProgressDialog.show(getActivity(), "", "Connecting...", true);
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				final ArrayList<Stock> newData = mService.getNSEData();
				
				h.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						dialog.dismiss();
						orders = newData;
						adapter = new NSEAdapter(getActivity(), orders);
						setListAdapter(adapter);
						
					}
					
				});
				
			}
			
		});
		
		thread.start();
	}
	
	

}
