package com.novatia.leadtrader.base;

//import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.novatia.leadtrader.activity.AddFundActivity;
import com.novatia.leadtrader.activity.DashboardActivity;
import com.novatia.leadtrader.activity.FeedbackActivity;
import com.novatia.leadtrader.activity.LoginActivity;
import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.activity.RequestFundActivity;
import com.novatia.leadtrader.activity.SettingsActivity;
import com.novatia.leadtrader.activity.StatementActivity;
import com.novatia.leadtrader.activity.UpdateActivity;


public class LayoutDrawerActionBarActivity extends ActionBarActivity{
	public String[] menuTitles;
	public DrawerLayout mDrawerLayout;
	public ListView mDrawerList;
	public CharSequence mTitle;
	public ActionBarDrawerToggle mDrawerToggle;
	
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer_menu);
		
	}
	
	public void setDrawerData(){
		mTitle = getString(R.string.app_name);
		menuTitles = getResources().getStringArray(R.array.nav_menu_items);
//		menuTitles = new String[]{"One", "Two", "Three"};
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		mDrawerList = (ListView)findViewById(R.id.drawer_list);
		
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.menu_item, menuTitles);
		mDrawerList.setAdapter(adapter);
		
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
		 
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer_new,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {
 
            /** Called when a drawer has settled in a completely closed state. */
            @Override
			public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
 
            /** Called when a drawer has settled in a completely open state. */
            @Override
			public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }
        };
 
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
 @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {      	
            return true;
        }
        // Handle your other action bar items...
 
        return super.onOptionsItemSelected(item);
    }
    
    public void selectItem(int pos){
//    	Toast.makeText(this, R.string.app_name, Toast.LENGTH_SHORT).show();
    	mDrawerList.setItemChecked(pos, true);
		mDrawerLayout.closeDrawer(mDrawerList);
    	switch(pos){
    	
    	case 0:
    		goToDashboard();
    		break;
    	case 1:
    		goToStatementAccount();
    		break;
    	case 2:
    		goToRequestFund();
    		break;
    	case 3:
    		goToAddFund();
    		break;
    	case 4:
    		goToUpdate();
    		break;
    	case 5:
    		goToFeedback();
    		break;
    	case 6:
    		goToSettings();
    		break;
    	case 7:
    		logout();
    		break;
    	default:
    		
    	}
    }
    
    public void goToRequestFund(){
    	if(mTitle.equals("Request Fund"))
    		return;
    	Intent i = new Intent(this, RequestFundActivity.class);
		mTitle = "Request Fund";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
    	
    }
    
    public void goToAddFund(){
    	if(mTitle.equals("Add Fund"))
    		return;
    	Intent i = new Intent(this, AddFundActivity.class);
		mTitle = "Add Fund";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
    	
    }
    
    public void goToDashboard(){
    	if(mTitle.equals("Dashboard"))
    		return;
    				
		Intent i = new Intent(this, DashboardActivity.class);
		mTitle = "Dashboard";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
		
	}
	
	public void goToSettings(){
		if(mTitle.equals("Settings"))
    		return;
		
		Intent i = new Intent(this, SettingsActivity.class);
		
		mTitle = "Settings";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
	}
	
	public void goToDailyPosCert(){
//		if(mTitle.equals("Daily Pos Cert"))
//    		return;
//		
//		Intent i = new Intent(this, DailyPosCertActivity.class);
//		
//		mTitle = "Daily Pos Cert";
//		getSupportActionBar().setTitle(mTitle);
//		startActivity(i);
	}
	
	public void goToStatementAccount(){
		if(mTitle.equals("Statement of Account"))
    		return;
		
		Intent i = new Intent(this, StatementActivity.class);
		
		mTitle = "Statement of Account";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
	}
	
	public void goToUpdate(){
		if(mTitle.equals("News and Updates"))
    		return;
		
		Intent i = new Intent(this, UpdateActivity.class);
		
		mTitle = "News and Updates";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
	}
	
	public void goToFeedback(){
		if(mTitle.equals("News and Updates"))
    		return;
		
		Intent i = new Intent(this, FeedbackActivity.class);
		
		mTitle = "Feedback";
		getSupportActionBar().setTitle(mTitle);
		startActivity(i);
	}
	
	public void logout(){
		Intent i = new Intent(this, LoginActivity.class);
		startActivity(i);
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
           selectItem(position);
        	
        }
    }
	
	
	

	
}
