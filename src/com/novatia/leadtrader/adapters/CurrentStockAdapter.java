package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.fragment.NewOrderFragment;
import com.novatia.lc.types.Stock;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


public class CurrentStockAdapter extends ArrayAdapter<Stock> implements OnClickListener{
	
	private final LayoutInflater mInflater;
	
	TextView stockId;
	TextView stockUnits;
	TextView stockValue;
	TextView boughtPriceTextView;
	
	Button orderButton;
	
	ArrayList<Stock> gStock;
	
	Stock st;
	
	
	public CurrentStockAdapter(Context context, ArrayList<Stock> stocks){
		super(context, R.layout.stock_current_list_item, stocks);
		gStock = stocks;
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent){
		st = getItem(pos);
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.stock_current_list_item, parent, false);
			
		}
		
		stockId = (TextView)convertView.findViewById(R.id.stockTitleTextView);
		stockUnits = (TextView)convertView.findViewById(R.id.stockTotalUnitsTextView);
		stockValue = (TextView)convertView.findViewById(R.id.stockValueTextView);
		boughtPriceTextView = (TextView)convertView.findViewById(R.id.boughtPriceTextView);
		
		orderButton = (Button)convertView.findViewById(R.id.orderButton);
		orderButton.setTag(Integer.toString(pos));
		orderButton.setOnClickListener(this);
		
		
		stockId.setText(st.getInstrumentId());
		stockValue.setText("N"+st.getValue());
		stockUnits.setText(st.getQuantity()+" shares");
		boughtPriceTextView.setText(st.getPrice());
		
		
		return convertView;
	}
	
	public void showOrderDialog(String ref, String price, String side){
		Bundle bundle = new Bundle();
		bundle.putString("ref", ref);
		bundle.putString("price", price);
		bundle.putString("side", side);
		
		FragmentActivity activity = ((FragmentActivity)getContext());
		FragmentManager fragment = activity.getSupportFragmentManager();
		NewOrderFragment orderDialog = new NewOrderFragment();
		orderDialog.setArguments(bundle);
		orderDialog.show(fragment, "New Order");
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Stock sStock = gStock.get(Integer.parseInt((String)arg0.getTag()));
		
		showOrderDialog(sStock.getInstrumentId(), sStock.getPrice(), "");
		
	}

	
	
	
}