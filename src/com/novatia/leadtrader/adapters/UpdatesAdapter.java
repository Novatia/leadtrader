package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.types.Update;

import com.novatia.leadtrader.activity.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UpdatesAdapter extends ArrayAdapter<Update>{
	
	public UpdatesAdapter(Context context, ArrayList<Update> updates){
		super(context, R.layout.updates_list_item, updates);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		Update update = getItem(position);
		
		if(convertView == null){
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.updates_list_item, parent, false);
		}
		
		TextView updatesTitle = (TextView)convertView.findViewById(R.id.updatesListTitleTextView);
		TextView updatesContent = (TextView)convertView.findViewById(R.id.updatesListContentTextView);
		
		updatesTitle.setText(update.getTitle());
		updatesContent.setText(update.getContent());
		
		return convertView;
	}
}
