package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.lc.types.News;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NewsAdapter extends ArrayAdapter<News>{

	public NewsAdapter(Context context, ArrayList<News> news){
		super(context, R.layout.stock_news_list_item, news);
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent){
		
		if(convertView == null){
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.news_list_item, parent, false);
		}
		
		News newsItem = getItem(pos);
		
		
		TextView newsItemTitleTextView = (TextView)convertView.findViewById(R.id.newsItemTitle);
		TextView newsItemSourceTextView = (TextView)convertView.findViewById(R.id.newsItemSource);
		TextView newsItemDateTextView = (TextView)convertView.findViewById(R.id.newsItemDate);
		
		newsItemTitleTextView.setText(newsItem.getTitle());
		newsItemSourceTextView.setText(newsItem.getSource());
		newsItemDateTextView.setText(newsItem.getDate());
		
	
		return convertView;
	}
	
	
	
}
