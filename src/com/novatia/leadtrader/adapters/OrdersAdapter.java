package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.lc.helper.DecimalPlaces;
import com.novatia.lc.types.Order;
import com.novatia.lc.types.OrderStatus;
import com.novatia.lc.types.Side;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class OrdersAdapter extends ArrayAdapter<Order>{
	
	private final LayoutInflater mInflater;
	private String type;

	public OrdersAdapter(Context context, ArrayList<Order> orders){
		
		super(context, R.layout.orders_list_item, orders);
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent){
		Order order = getItem(pos);
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.orders_list_item, parent, false);
			
		}
		
		TextView titleHolder = (TextView)convertView.findViewById(R.id.titleHolder);
		TextView unitPriceTextView = (TextView)convertView.findViewById(R.id.unitPriceTextView);
		TextView quantityTextView = (TextView)convertView.findViewById(R.id.quantityTextView);
		TextView valueTextView = (TextView)convertView.findViewById(R.id.valueTextView);
		TextView bTextView = (TextView)convertView.findViewById(R.id.bTextView);
		TextView statusTextView = (TextView)convertView.findViewById(R.id.statusTextView);
		TextView dateTextView = (TextView)convertView.findViewById(R.id.dateHolder);
		
		titleHolder.setText(order.getInstrumentId());
		
		double  p = DecimalPlaces.round(Double.parseDouble(order.getPrice()), 2);
		unitPriceTextView.setText("N"+ Double.toString(p));
		quantityTextView.setText(order.getQuantity());
		
		double  v = DecimalPlaces.round(Double.parseDouble(order.getValue()), 2);
		valueTextView.setText("N"+Double.toString(v));
		
		bTextView.setText(Side.getSide(order.getSide()));
		statusTextView.setText(OrderStatus.getStatus(order.getTradingStatus()));
		
		String dt = order.getTransactionDate().substring(0, 8);
		dateTextView.setText(dt);
		
		return convertView;
	}
	
}
