package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.lc.types.Statement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StatementAdapter extends ArrayAdapter<Statement>{
	private final LayoutInflater mInflater;
	
	
	public StatementAdapter(Context context, ArrayList<Statement> statements) {
		super(context, R.layout.statement_list_item, statements);
		// TODO Auto-generated constructor stub
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent){
		Statement statement = getItem(pos);
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.statement_list_item, parent, false);
			
		}
		
		TextView refNo = (TextView)convertView.findViewById(R.id.refNoHolder);
		TextView date = (TextView)convertView.findViewById(R.id.dateHolder);
		TextView secCode = (TextView)convertView.findViewById(R.id.secCodeTextView);
		TextView units = (TextView)convertView.findViewById(R.id.unitsTextView);
		TextView price = (TextView)convertView.findViewById(R.id.priceTextView);
		TextView debit = (TextView)convertView.findViewById(R.id.debitTextView);
		TextView credit = (TextView)convertView.findViewById(R.id.creditTextView);
		TextView balance = (TextView)convertView.findViewById(R.id.balanceTextView);
		
		refNo.setText(statement.getUniqueId());
		date.setText(statement.getTransactionDate());
		secCode.setText(statement.getInstrumentId());
		units.setText(statement.getQuantity());
		price.setText(statement.getPrice());
		debit.setText(statement.getDebitValue());
		credit.setText(statement.getCreditValue());
		balance.setText(statement.getBalance());
		
		return convertView;
	}

}
