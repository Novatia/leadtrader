package com.novatia.leadtrader.adapters;

import java.util.ArrayList;

import com.novatia.leadtrader.activity.R;
import com.novatia.leadtrader.fragment.NewOrderFragment;
import com.novatia.lc.types.Stock;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class NSEAdapter extends ArrayAdapter<Stock> implements OnClickListener, Filterable{
	
	private final LayoutInflater mInflater;
	
	NSEFilter nseFilter;
	
	Button orderButton;
	ArrayList<Stock> gStock;
	ArrayList<Stock> database;
	public NSEAdapter(Context context, ArrayList<Stock> stocks) {
		
		super(context, R.layout.nse_list_item, stocks);
		gStock = stocks;
		database = stocks;
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// TODO Auto-generated constructor stub
		nseFilter = new NSEFilter();
	}
	
	
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent){
		Stock stock = gStock.get(pos);
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.nse_list_item, parent, false);
			
		}
		
		TextView nameHolder = (TextView)convertView.findViewById(R.id.nameHolder);
		TextView titleHolder = (TextView)convertView.findViewById(R.id.titleHolder);
		TextView unitPriceTextView = (TextView)convertView.findViewById(R.id.todayPriceTextView);
		TextView yesterdayPriceTextView = (TextView)convertView.findViewById(R.id.yesterdayPriceTextView);
		
		nameHolder.setText(stock.getInstrumentId());
		titleHolder.setText(stock.getInstrumentId());
		unitPriceTextView.setText("N"+stock.getPrice());
//		yesterdayPriceTextView.setText("N"+stock.getClosingPrices().getClosePrice());
		
		orderButton = (Button)convertView.findViewById(R.id.orderButton);
		orderButton.setTag(Integer.toString(pos));
		orderButton.setOnClickListener(this);
		
		
		return convertView;
	}
	
	public void showOrderDialog(String ref, String price, String side){
		Bundle bundle = new Bundle();
		bundle.putString("ref", ref);
		bundle.putString("price", price);
		bundle.putString("side", side);
		
		FragmentActivity activity = ((FragmentActivity)getContext());
		FragmentManager fragment = activity.getSupportFragmentManager();
		NewOrderFragment orderDialog = new NewOrderFragment();
		orderDialog.setArguments(bundle);
		orderDialog.show(fragment, "New Order");
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Stock sStock = gStock.get(Integer.parseInt((String)arg0.getTag()));
		
		showOrderDialog(sStock.getInstrumentId(), sStock.getPrice(), "");
		
	}
	
	@Override
	public Filter getFilter(){
		
			
			
		return nseFilter;
		
	}
	
	 @Override
	    public int getCount() {
	        return gStock.size();
	    }
	 
	 @Override
	 public Stock getItem(int position) {
	     return gStock.get(position);
	 }
	
	
	private class NSEFilter extends Filter{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			
			final FilterResults results = new FilterResults();
			final ArrayList<Stock> fStocks = new ArrayList<Stock>();
			
			Log.i("first one, length: ", Integer.toString(constraint.length()));
			
			if(constraint == null || constraint.length() == 0){
				gStock = database;
				results.values = gStock;
				results.count = gStock.size();
				
			}else{
				
				for(Stock t: database){
					if(t.getInstrumentId().startsWith(constraint.toString().toUpperCase())){
						Log.i("constriaint", constraint.toString());
						Log.i("instrunment id :: ", t.getInstrumentId());
						fStocks.add(t);
					}
					
					
				}
				results.values = fStocks;
				results.count = fStocks.size();
				
			}
			
			
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
			if(results.count == 0){
				notifyDataSetInvalidated();
			}else{
				gStock = (ArrayList<Stock>) results.values;
				notifyDataSetChanged();
				
			}
			
			
		}
		
	}

}
