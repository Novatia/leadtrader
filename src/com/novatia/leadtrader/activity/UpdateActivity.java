package com.novatia.leadtrader.activity;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.novatia.lc.api.BackEndService;
import com.novatia.lc.api.XmlParser;
import com.novatia.leadtrader.adapters.NewsAdapter;
import com.novatia.leadtrader.base.LayoutDrawerActivity;
import com.novatia.lc.types.News;

public class UpdateActivity extends LayoutDrawerActivity implements OnItemClickListener{
	
	ListView updatesList;
	ArrayList<News> updates;
	
	
	Handler handler;
	
	ProgressDialog dialog;
	
	NewsAdapter adapter;

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_updates);
		setDrawerData();
		getActionBar().setIcon(android.R.color.transparent);
		
		updates = new ArrayList<News>();
		updatesList = (ListView)findViewById(R.id.updatesList);
		
		adapter = new NewsAdapter(this, updates);
		updatesList.setAdapter(adapter);
		
		updatesList.setOnItemClickListener(this);
		getActionBar().setTitle("News and Updates");
		
		displayUpdates();
	}
	
	
	

	
	public void displayUpdates(){
		handler = new Handler();
		dialog = ProgressDialog.show(this, "", "Connecting...", true);
		
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				BackEndService serv = new BackEndService();
				
				String n;
				try{
					 n = serv.getBackEndUpdates();
					 final ArrayList<News> news = new XmlParser().parseUpdates(n);
					 handler.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							dialog.dismiss();
							updates = news;
							adapter = new NewsAdapter(UpdateActivity.this, updates);
							updatesList.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							
						}
						 
					 });
					 
					
				}catch(Exception e){
					e.printStackTrace();
					dialog.dismiss();
				}
				
				
			}
			
		});
		thread.start();
		
	}





	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		News itemClicked = updates.get(arg2);
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemClicked.getLink()));
		startActivity(browserIntent);
		
	}
	

}
