package com.novatia.leadtrader.activity;

import com.novatia.lc.service.LCService;
import com.novatia.lc.types.User;
import com.novatia.leadtrader.base.LayoutDrawerActionBarActivity;
import com.novatia.leadtrader.classes.SupportFragmentTabListener;
import com.novatia.leadtrader.fragment.DashboardHistoricFragment;
import com.novatia.leadtrader.fragment.DashboardNSEFragment;
import com.novatia.leadtrader.fragment.DashboardCurrentFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;
import android.widget.TextView;


public class DashboardActivity extends LayoutDrawerActionBarActivity{
	
	LCService s;
	TextView stockValueText;
	TextView cashBalanceText;
	TextView accountNameText;
	TextView accountNumberText;
	TextView dateHolder;
	
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
	      setUserDetails(s.getCurrentUser());
	    
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
	    }
	  };
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);
		setDrawerData();
		getActionBar().setIcon(android.R.color.transparent);
		
		setupTabs();
		getActionBar().setTitle(mTitle);
		
		stockValueText = (TextView)findViewById(R.id.accStockValue);
		accountNameText = (TextView)findViewById(R.id.accNameTextView);
		accountNumberText = (TextView)findViewById(R.id.accNumberTextView);
		cashBalanceText = (TextView)findViewById(R.id.accBalanceTextView);
		
		dateHolder = (TextView)findViewById(R.id.dateHolder);
	
	
	}
	
	private void setupTabs() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);

		Tab tab1 = actionBar
		    .newTab()
		    .setText("My Portfolio")
//		    .setIcon(R.drawable.play)
		    .setTag("HomeTimelineFragment")
		    .setTabListener(new SupportFragmentTabListener<DashboardCurrentFragment>(R.id.dashboardFragmentContainer, this,
	                    "current", DashboardCurrentFragment.class));

		actionBar.addTab(tab1);
		actionBar.selectTab(tab1);
		
//		Removed tab 2

//		Tab tab2 = actionBar
//		    .newTab()
//		    .setText("My Orders")
////		    .setIcon(R.drawable.current_stock)
//		    .setTag("MentionsTimelineFragment")
//		    .setTabListener(new SupportFragmentTabListener<DashboardNewFragment>(R.id.dashboardFragmentContainer, this,
//	                    "new", DashboardNewFragment.class));
//		actionBar.addTab(tab2);
		
		Tab tab3 = actionBar
			    .newTab()
			    .setText("Orders")
//			    .setIcon(R.drawable.historic)
			    .setTag("MentionsTimelineFragment")
			    .setTabListener(new SupportFragmentTabListener<DashboardHistoricFragment>(R.id.dashboardFragmentContainer, this,
		                    "historic", DashboardHistoricFragment.class));
			actionBar.addTab(tab3);
			
			Tab tab4 = actionBar
				    .newTab()
				    .setText("Market")
//				    .setIcon(R.drawable.nse)
				    .setTag("MentionsTimelineFragment")
				    .setTabListener(new SupportFragmentTabListener<DashboardNSEFragment>(R.id.dashboardFragmentContainer, this,
			                    "nse", DashboardNSEFragment.class));
				actionBar.addTab(tab4);
	}
	
	public void showDatePickerDialog(View v) {
	    
	}
	
	 @Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	 
	 @Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
	 
	 public void setUserDetails(User user){
		 if(user!=null){
			 
			 accountNameText.setText(user.getAccountName());
			 accountNumberText.setText(user.getAccountId());
			 
			
			 
			 stockValueText.setText(Double.toString(user.getAccountNav()));
			 cashBalanceText.setText(Double.toString(user.getCashBalance()));
			 dateHolder.setText(user.getValuationDate().toString());
			 
		 }
		 
	 }
	 
	
	

}
