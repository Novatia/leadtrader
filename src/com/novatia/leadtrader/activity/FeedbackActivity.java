package com.novatia.leadtrader.activity;


import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.leadtrader.base.LayoutDrawerActivity;

public class FeedbackActivity extends LayoutDrawerActivity{
	
	EditText subjectText;
	EditText summaryText;
	Button submitButton;
	
	LCService s;
	Handler h;
	ProgressDialog dialog;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
	      Toast.makeText(FeedbackActivity.this, "Connected", Toast.LENGTH_SHORT)
	          .show();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	    }
	  };
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		setDrawerData();
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(android.R.color.transparent);
		getActionBar().setTitle("Feedback");
		
		subjectText = (EditText)findViewById(R.id.subjectEditText);
		summaryText = (EditText)findViewById(R.id.messageEditText);
		submitButton = (Button)findViewById(R.id.sendFeedbackButton);
		
		submitButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				submitFeedBack();
				
			}
			
		});
		
		
	}
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
	
	public void submitFeedBack(){
		if(!isEmptyString(subjectText.getText().toString()) | !isEmptyString(summaryText.getText().toString())){
			
				if(InternetHelper.isOnline()){
					dialog = ProgressDialog.show(this, "", "Connecting...", true);
					
					h = new Handler();
					
					Thread thread = new Thread(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Log.i("Info", "It is in thread run");
							final boolean resp = s.sendFeedback(subjectText.getText().toString(), summaryText.getText().toString());
							
							h.post(new Runnable(){
								
								

								@Override
								public void run() {
									// TODO Auto-generated method stub
									subjectText.setText("");
									summaryText.setText("");
									Log.i("Info", "It is in handler post run");
									if(resp){
										Toast.makeText(FeedbackActivity.this, "Feedback submitted successfully.", Toast.LENGTH_LONG)
								          .show();
										
									}else{
										Toast.makeText(FeedbackActivity.this, "Please try again later.", Toast.LENGTH_LONG)
								          .show();
										
									}
									dialog.dismiss();
									Log.i("Info", "It is done");
									
								}
								
							});
							
						}
						
					});
					
					thread.start();
					
				}else{
					Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_LONG)
			          .show();
					
				}
				
			
			
			
		}else{
			 Toast.makeText(this, "Subject and Message can not be empty", Toast.LENGTH_LONG)
	          .show();
		}
		
	}
	
	public boolean isEmptyString(String s){
		if(s.equals(""))
			return true;
		return false;
	}
	
	 

}
