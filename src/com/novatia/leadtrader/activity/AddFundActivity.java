package com.novatia.leadtrader.activity;

import android.os.Bundle;

import com.novatia.leadtrader.base.LayoutDrawerActionBarActivity;

public class AddFundActivity extends LayoutDrawerActionBarActivity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_fund);
		setDrawerData();
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(android.R.color.transparent);
		getActionBar().setTitle("Add Fund");
	}
}
