package com.novatia.leadtrader.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.widget.TextView;

public class TermsActivity extends Activity{
	TextView termsTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms);
		getActionBar().setIcon(android.R.color.transparent);
		
		termsTextView = (TextView)findViewById(R.id.termsBox);
		termsTextView.setText(Html.fromHtml(getString(R.string.terms_and_conditions_full_text)));
		termsTextView.setClickable(true);
		termsTextView.setMovementMethod(LinkMovementMethod.getInstance());
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
