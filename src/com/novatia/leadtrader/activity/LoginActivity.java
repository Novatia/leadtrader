package com.novatia.leadtrader.activity;

import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.leadtrader.fragment.PasswordRequestDialog;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class LoginActivity extends FragmentActivity {
	
	CheckBox termsCheckBox;
	Button loginButton;
	TextView userIdText;
	TextView passwordText;
	TextView termsTextView;
	TextView createAccount;
	ProgressBar progressBar;
	CheckBox termsCheckbox;
	
	boolean loginClicked = false;
	
	
	
	
	private LCService s;
	
	private Handler handler;
	
	private String username = "";
	private String password = "";
	
	ProgressDialog dialog;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
	      loginButton.setEnabled(true);
//	      Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT)
//	          .show();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	    }
	  };
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		termsTextView = (TextView) findViewById(R.id.termsTextView);
		createAccount = (TextView)findViewById(R.id.createAccountTextView);
		termsTextView.setText(Html.fromHtml(getString(R.string.terms_and_conditions_text)));
		termsCheckBox = (CheckBox) findViewById(R.id.termsCheckBox);
		
		termsTextView.setClickable(true);
		termsTextView.setMovementMethod(LinkMovementMethod.getInstance());
		progressBar = (ProgressBar)findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.INVISIBLE);
		
		userIdText = (EditText)findViewById(R.id.useridText);
		passwordText = (EditText)findViewById(R.id.passwordText);
		
		
		loginButton = (Button)findViewById(R.id.loginButton);
		loginButton.setEnabled(false);
		
		createAccount.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String url = "http://leadtraderng.com/open-an-account/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
				
			}
			
		});
		
		setStoredUsername();
		
//		userIdText.setText()
		
		mStartService();
		
	}

	public void setStoredUsername(){
		SharedPreferences pref =   
			    PreferenceManager.getDefaultSharedPreferences(this);
			String username = pref.getString("username", "");
		userIdText.setText(username);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	
	public void showPasswordRequestDialog(View v){
		FragmentManager fragment = getSupportFragmentManager();
		PasswordRequestDialog passwordRequestDialog = PasswordRequestDialog.newInstance();
		passwordRequestDialog.show(fragment, "fragment_forgot_password");
	}
	
	public void Login(View v){
		if(loginClicked){
			return;
		}
		loginClicked = true;
		dialog = ProgressDialog.show(LoginActivity.this, "", "Connecting...", true);
		if(InternetHelper.isOnline()){
			username = userIdText.getText().toString().trim();
			password = passwordText.getText().toString().trim();
			handler = new Handler();
			
			if(!username.equals("") && !password.equals("")){
				saveUsername(username);

				if(termsCheckBox.isChecked()){
					
					Thread thread = new Thread(new Runnable(){
					    @Override
					    public void run() {
					        
					          final boolean r =	s.login(username, password);
					           handler.post(new Runnable(){
					        	   @Override
					        	   public void run(){
//					        		   progressBar.setVisibility(ProgressBar.INVISIBLE);
					        		   dialog.dismiss();
					        		   loginClicked = false;
					        		   if (r){
					        			   
					        		   goToDashboard();
					        		   }else{
					        			   
					        			   Toast.makeText(LoginActivity.this, "Incorrect username or password.", Toast.LENGTH_LONG)
					     		          .show();
					        			   
					        		   }
					        	   }
					           }); 
					        
					    }
					});
					thread.start();
				}else{
					dialog.dismiss();
					 Toast.makeText(LoginActivity.this, "Check terms and conditions.", Toast.LENGTH_LONG)
    		          .show();
				}
				
			}else{
				dialog.dismiss();
				Toast.makeText(LoginActivity.this, "Check your username and password.", Toast.LENGTH_LONG)
		          .show();
			}
			
		}else{
			dialog.dismiss();
		      Toast.makeText(LoginActivity.this, "Please check your Internet", Toast.LENGTH_LONG)
          .show();
			
		}
		
		loginClicked = false;
		
	}
	
	
	public void disableViews(){
		
	}
	
	public void saveUsername(String username){
		SharedPreferences pref =   
			    PreferenceManager.getDefaultSharedPreferences(this);
			Editor edit = pref.edit();
			edit.putString("username", username);
			edit.commit(); 
	}
	
	
	public void goToDashboard(){
		Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
		startActivity(i);
	}
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
	
	private void mStartService(){
		Intent service = new Intent(this, LCService.class);
	    startService(service);
	}
	
	
	
	public static void setActivityEnabled(Context context,final Class<? extends Activity> activityClass,final boolean enable)
    {
    final PackageManager pm=context.getPackageManager();
    final int enableFlag=enable ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
    pm.setComponentEnabledSetting(new ComponentName(context,activityClass),enableFlag,PackageManager.DONT_KILL_APP);
    }
	
	
	
	
	
	


}


