package com.novatia.leadtrader.activity;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.novatia.lc.api.GoogleNews;
import com.novatia.lc.api.XmlParser;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.InstrunmentPrices;
import com.novatia.lc.types.News;
import com.novatia.lc.types.Price;
import com.novatia.leadtrader.adapters.NewsAdapter;
import com.novatia.leadtrader.base.LayoutDrawerActivity;
import com.novatia.leadtrader.fragment.NewOrderFragment;

public class StockItemActivity extends LayoutDrawerActivity implements OnClickListener, OnItemClickListener{
//	ArrayList<News> news;
	TextView newsTextView;
	
	LCService s;
	
	Button buyButton;
	Button sellButton;
	
//	ArrayList<News> latestNews;
	
	String tinker;
	String price;
	
	TextView priceTextView;
	TextView tinkerTextView;
	
	Handler handler;
	
	NewsAdapter adapter;
	
	ProgressDialog dialog;
	
	ListView newsListView;
	
	LineChart mLineChart;
	
	boolean mBound = false;
	
	ArrayList<News> newsTicker = new ArrayList<News>();
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
//	      loginButton.setEnabled(true);
//	      Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT)
//	          .show();
	      if(!mBound)
	    	  getStockPrices();
	      mBound = true;
//	      newsTicker();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	      mBound = false;
	    }
	  };
	
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stockitem);
		
		setDrawerData();
//		getActionBar().setIcon(android.R.color.transparent);
//		getActionBar().setIcon(android.R.color.transparent);
		
		
		newsListView = (ListView)findViewById(R.id.newsListView);
		
		adapter = new NewsAdapter(this, newsTicker);
		newsListView.setAdapter(adapter);
		newsListView.setOnItemClickListener(this);
		
		Bundle extras = getIntent().getExtras();
		tinker = extras.getString("ref");
		price = extras.getString("price");
		
		priceTextView = (TextView)findViewById(R.id.stockItemPricetextView);
		tinkerTextView = (TextView)findViewById(R.id.stockNameTextView);
		
		buyButton = (Button)findViewById(R.id.buyButton);
		sellButton = (Button)findViewById(R.id.sellButton);
		
		mLineChart = (LineChart)findViewById(R.id.chart);
		mLineChart.setDescription("5 days prices");
		mLineChart.setDrawYValues(true);
		mLineChart.setDrawYLabels(true);
		
		buyButton.setOnClickListener(this);
		sellButton.setOnClickListener(this);
		
		priceTextView.setText(price);
		tinkerTextView.setText(tinker);
		
		
		
		
		
		
//		createHoloGraph();
		
	}
	
	public void showOrderDialog(String ref, String price, String side){
		Bundle bundle = new Bundle();
		bundle.putString("ref", ref);
		bundle.putString("price", price);
		bundle.putString("side", side);
		
		FragmentManager fragment = getSupportFragmentManager();
		NewOrderFragment orderDialog = new NewOrderFragment();
		orderDialog.setArguments(bundle);
		orderDialog.show(fragment, "New Order");
	}
	
	
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
	

	public void getStockPrices(){
		
		handler = new Handler();
		dialog = ProgressDialog.show(this, "", "Connecting...", true);
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				final ArrayList<Price> prices = s.getFiveDaysPriceListOf(tinker);
				try {
					GoogleNews newsList = new GoogleNews(tinker);
					final ArrayList<News> latestNews = new XmlParser().parseNews(newsList.getRSS());
					
					handler.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							dialog.dismiss();
							
//							createHoloGraph(prices);
							createLineChart(prices);
							newsTicker = latestNews;
							adapter = new NewsAdapter(StockItemActivity.this, newsTicker);
							newsListView.setAdapter(adapter);
							adapter.notifyDataSetChanged();
//							dialog.dismiss();
							
							
						}
						
					});
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					handler.post(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							dialog.dismiss();
//							createHoloGraph(prices);
							createLineChart(prices);
							
							
							
							
						}
						
					});
				}
				
				
				
			}
			
		});
		
		thread.start();
		
	}
	
	public void createHoloGraph(InstrunmentPrices prices){
//		Line l = new Line();
//		LinePoint p = new LinePoint();
//		p.setX(0);
//		if(prices.getLastQuarterClosePrice()!=null){
//		p.setY(Double.parseDouble(prices.getLastQuarterClosePrice()));
//		}else{
//			p.setY(0);	
//		}
//		l.addPoint(p);
//		p = new LinePoint();
//		
//		p.setX(4);
//		if(prices.getLastMonthClosePrice()!=null){
//		p.setY(Double.parseDouble(prices.getLastMonthClosePrice()));
//		}else{
//			p.setY(0);
//		}
//		l.addPoint(p);
//		p = new LinePoint();
//		
//		p.setX(8);
//		if(prices.getLastWeekClosePrice()!=null){
//		p.setY(Double.parseDouble(prices.getLastWeekClosePrice()));
//		}else{
//			p.setY(0);
//		}
//		l.addPoint(p);
//		
//		p = new LinePoint();
//		p.setX(12);
//		if(prices.getClosePrice() != null){
//		p.setY(Double.parseDouble(prices.getClosePrice()));
//		}else{
//			p.setY(0);
//		}
//		l.addPoint(p);
//		
//		
//		l.setColor(Color.parseColor("#FFBB33"));
//
//		LineGraph li = (LineGraph)findViewById(R.id.stockItemGraphLinearLayout);
//		li.addLine(l);
//		li.setRangeY(0, 10);
//		li.setLineToFill(0);
	}
	
	
	public void createLineChart(ArrayList<Price> prices){
		ArrayList<Entry> valsStock = new ArrayList<Entry>();
		ArrayList<String> xVals = new ArrayList<String>();
		
		int counter = 0;
		for(Price p: prices){
			Entry entry = new Entry((float)p.getPrice(), counter);
			valsStock.add(entry);
			xVals.add(p.getDate().toString());
			counter++;
		}
		
		LineDataSet stockG = new LineDataSet(valsStock, "5 days");
		
		ArrayList<LineDataSet> dataSet = new ArrayList<LineDataSet>();
		dataSet.add(stockG);
		
		
		LineData data = new LineData(xVals, dataSet);
		mLineChart.setData(data);
		mLineChart.invalidate();
		
	}
	
	 
//	  public void onItemClick(ListView l, View v, int position, long id) {  
//		
//	    News itemClicked = newsTicker.get(position);
//		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemClicked.getLink()));
//		startActivity(browserIntent);
////		Toast.makeText(getActivity().getApplicationContext(), "It works", 
////				   Toast.LENGTH_LONG).show();
//	  }
	
	
	
//	public void newsTicker(){
//		handler = new Handler();
//		
//		dialog = ProgressDialog.show(this, "", "Connecting", true);
//		Thread thread = new Thread(new Runnable(){
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				
//				try {
//					GoogleNews newsList = new GoogleNews(tinker);
//					final ArrayList<News> latestNews = new XmlParser().parseNews(newsList.getRSS());
//					handler.post(new Runnable(){
//
//						@Override
//						public void run() {
//							// TODO Auto-generated method stub
//							adapter = new NewsAdapter(StockItemActivity.this, latestNews);
//							newsListView.setAdapter(adapter);
//							adapter.notifyDataSetChanged();
//							dialog.dismiss();
//							
//							
//						}
//						
//					});
//					
//					
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
////					dialog.dismiss();
//				}
//				
//			}
//			
//		});
//		
//		thread.start();
//		
//		
//	}

	@Override
	public void onClick(View arg) {
		// TODO Auto-generated method stub
		if(arg.getId() == R.id.sellButton){
			showOrderDialog(tinker, price, "sell");
		}else{
			showOrderDialog(tinker, price, "buy");
		}
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		News itemClicked = newsTicker.get(arg2);
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemClicked.getLink()));
		startActivity(browserIntent);
		
	}

}
