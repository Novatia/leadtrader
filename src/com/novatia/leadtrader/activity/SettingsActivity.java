package com.novatia.leadtrader.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;

import com.novatia.lc.service.LCService;
import com.novatia.leadtrader.base.LayoutDrawerActionBarActivity;
import com.novatia.leadtrader.classes.SupportFragmentTabListener;
import com.novatia.leadtrader.fragment.SettingsPasswordFragment;
import com.novatia.leadtrader.fragment.SettingsProfileFragment;

public class SettingsActivity extends LayoutDrawerActionBarActivity{
	
	LCService s;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
	      
//	      Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT)
//	          .show();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	    }
	  };
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		setDrawerData();
		getActionBar().setIcon(android.R.color.transparent);
		setupTabs();
		getActionBar().setTitle("Settings");
	}
	
	private void setupTabs() {
	ActionBar actionBar = getSupportActionBar();
	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	actionBar.setDisplayShowTitleEnabled(true);

	Tab tab1 = actionBar
	    .newTab()
	    .setText("Profile")
//	    .setIcon(R.drawable.ic_home)
	    .setTag("ProfileFragment")
	    .setTabListener(new SupportFragmentTabListener<SettingsProfileFragment>(R.id.settingsFragmentContainer, this,
                    "first", SettingsProfileFragment.class));

	actionBar.addTab(tab1);
	actionBar.selectTab(tab1);

	Tab tab2 = actionBar
	    .newTab()
	    .setText("Password")
//	    .setIcon(R.drawable.ic_mentions)
	    .setTag("PasswordFragment")
	    .setTabListener(new SupportFragmentTabListener<SettingsPasswordFragment>(R.id.settingsFragmentContainer, this,
                "first", SettingsPasswordFragment.class));
	actionBar.addTab(tab2);
}
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
}
