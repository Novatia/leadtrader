package com.novatia.leadtrader.activity;

import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.service.LCService;
import com.novatia.leadtrader.base.LayoutDrawerActionBarActivity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RequestFundActivity extends LayoutDrawerActionBarActivity{
	
	
	LCService s;
	Handler h;
	ProgressDialog dialog;
	
	EditText amountText;
	Button submitButton;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
//	      Toast.makeText(RequestFundActivity.this, "Connected", Toast.LENGTH_SHORT)
//	          .show();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	    }
	  };

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request_fund);
		setDrawerData();
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(android.R.color.transparent);
		getActionBar().setTitle("Request Fund");
		
		amountText = (EditText)findViewById(R.id.requestedAmountEditText);
		submitButton = (Button)findViewById(R.id.requestFundButton);
		
		submitButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				sendFundRequest();
				
			}
			
		});
		
		
	}
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }
	
	public void sendFundRequest(){
		if(!amountText.getText().toString().equals("")){
			
			if(InternetHelper.isOnline()){
				
				dialog = ProgressDialog.show(this, "", "Connecting...", true);
				h = new Handler();
				Thread thread = new Thread(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						final boolean resp = s.sendFundRequest(amountText.getText().toString());
						
						h.post(new Runnable(){

							@Override
							public void run() {
								// TODO Auto-generated method stub
								amountText.setText("");
								dialog.dismiss();
								
								if(resp){
									Toast.makeText(RequestFundActivity.this, "Successfully submitted.", Toast.LENGTH_LONG);
								}else{
									Toast.makeText(RequestFundActivity.this, "Successfully submitted.", Toast.LENGTH_LONG);
								}
							}
							
						});
						
						
					}
					
				});
				
				thread.start();
				
				
			}else{
				Toast.makeText(this, "Check your internet connection.", Toast.LENGTH_LONG).show();
				
			}
			
			
			
			
		}else{
			Toast.makeText(this, "Amount must be specified.", Toast.LENGTH_LONG).show();
		}
	}
	
	
}
