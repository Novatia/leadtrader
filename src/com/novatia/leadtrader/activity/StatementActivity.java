package com.novatia.leadtrader.activity;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.ListView;

import com.novatia.leadtrader.adapters.StatementAdapter;
import com.novatia.leadtrader.base.LayoutDrawerActivity;
import com.novatia.lc.service.LCService;
import com.novatia.lc.types.Statement;

public class StatementActivity extends LayoutDrawerActivity{
	ListView statementsList;
	
	LCService s;
	Handler handler;
	
	StatementAdapter adapter;
	
	ArrayList<Statement> tmpStatements = new ArrayList<Statement>();
	
	ArrayList<Statement> statements;
	
	ProgressDialog dialog;
	
	
	private ServiceConnection mConnection = new ServiceConnection() {

	    @Override
		public void onServiceConnected(ComponentName className, 
	        IBinder binder) {
	      LCService.MyBinder b = (LCService.MyBinder) binder;
	      s = b.getService();
	      setStatements();
	      
//	      loginButton.setEnabled(true);
//	      Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT)
//	          .show();
	    }

	    @Override
		public void onServiceDisconnected(ComponentName className) {
	      s = null;
//	      Toast.makeText(LoginActivity.this, "DisConnected", Toast.LENGTH_SHORT)
//          .show();
	    }
	  };
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statement);
		setDrawerData();
		getActionBar().setIcon(android.R.color.transparent);
		
		statements = new ArrayList<Statement>();
		
		statementsList = (ListView)findViewById(R.id.statementListView);
		
		adapter = new StatementAdapter(this, statements);
		statementsList.setAdapter(adapter);
		getActionBar().setTitle("Statement of Account");
	}
	
	
	public void setStatements(){
		
		handler = new Handler();
		
		dialog = ProgressDialog.show(this, "", "Connecting...", true);
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				
				tmpStatements = s.getStatements();
				
				handler.post(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						dialog.dismiss();
						statements = tmpStatements;
						updateListView();
						
					}
					
				});
				
			}
			
			
		});
		thread.start();
	}
	
	public void updateListView(){
		adapter = new StatementAdapter(this, statements);
		statementsList.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	
	@Override
	  protected void onPause() {
	    super.onPause();
	    unbindService(mConnection);
	  }
	
	@Override
	  protected void onResume() {
	    super.onResume();
	    Intent intent= new Intent(this, LCService.class);
	    bindService(intent, mConnection,
	        Context.BIND_AUTO_CREATE);
	  }

}
