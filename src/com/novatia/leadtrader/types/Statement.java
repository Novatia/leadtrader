package com.novatia.leadtrader.types;

import java.util.ArrayList;

public class Statement {

	public String ref;
	public int units;
	public String secCode;
	public String date;
	public String reference;
	
	public double debit;
	public double credit;
	public double balance;
	public double price;
	
	
	public Statement(String ref, String reference, String secCode, String date, int units, double price, double debit, double credit, double balance){
		
		this.ref = ref;
		this.secCode = secCode;
		this.date = date;
		
		this.units = units;
		
		this.credit = credit;
		this.debit = debit;
		this.balance = balance;
		
		this.reference = reference;
		this.price = price;
	}
	
	public static ArrayList<Statement> getAccountStatements(){
		ArrayList<Statement> statements = new ArrayList<Statement>();
		
		statements.add(new Statement("90781", "Sales", "NAHCO", "8/21/2004", 1100, 4.91, 0.00, 5301, -156780));
		statements.add(new Statement("90791", "Sales Proceed", "LIVESTOCK", "8/21/2004", 1100, 2.01, 0.00, 501, -5670));
		statements.add(new Statement("90771", "Sales", "NAHCO", "8/21/2004", 1600, 7.0, 0.00, 531, -15780));
		
		return statements;
	}
	
}
