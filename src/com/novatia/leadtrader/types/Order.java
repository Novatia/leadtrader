package com.novatia.leadtrader.types;

import java.util.ArrayList;

public class Order {
	
	private String date;
	private String name;
	private String buy_or_sell;
	private String trading_status;
	
	private int quantity;
	private double unit_price;
	private double value;
	
	
	public Order(String date, String name, String b_o_s, String trading, int q, double u){
		this.date = date;
		this.name = name;
		this.buy_or_sell = b_o_s;
		this.trading_status = trading;
		
		this.quantity = q;
		
		this.unit_price = u;
		this.value = Math.round(this.unit_price * this.quantity);
	}
	
	
	public static ArrayList<Order> getDummyOrders(){
		ArrayList<Order> orders = new ArrayList<Order>();
		
		orders.add(new Order("1/10/2014", "OANDO PLC", "Buy", "Complete", 2, 26.90));
		orders.add(new Order("3/6/2014", "COSTAIN PLC", "Buy", "Filled", 5000, 1.67));
		orders.add(new Order("1/9/2014", "AFROOIL PLC", "Buy", "Booked", 2, 20.70));
		orders.add(new Order("1/10/2014", "NOVATIA LTD", "Buy", "Complete", 4000, 1.07));
		
		return orders;
	}
	
	public static ArrayList<Order> getDummyHistoricOrders(){
		ArrayList<Order> orders = new ArrayList<Order>();
		
		orders.add(new Order("1/10/2014", "OANDO PLC", "Buy", "Complete", 2, 26.90));
		orders.add(new Order("3/6/2014", "COSTAIN PLC", "Buy", "Filled", 5000, 1.67));
		orders.add(new Order("1/9/2014", "AFROOIL PLC", "Buy", "Booked", 2, 20.70));
		orders.add(new Order("1/10/2014", "NOVATIA LTD", "Buy", "Complete", 4000, 1.07));
		
		orders.add(new Order("1/10/2014", "ACCESSBANK", "Buy", "Complete", 2, 26.90));
		orders.add(new Order("3/6/2014", "DIAMONDBANK PLC", "Buy", "Filled", 5000, 1.67));
		orders.add(new Order("1/9/2014", "CWG PLC", "Buy", "Booked", 2, 20.70));
		orders.add(new Order("1/10/2014", "OANDO PLC", "Buy", "Complete", 4000, 1.07));
		
		orders.add(new Order("1/10/2014", "OANDO PLC", "Buy", "Complete", 2, 26.90));
		orders.add(new Order("3/6/2014", "COSTAIN PLC", "Buy", "Filled", 5000, 1.67));
		orders.add(new Order("1/9/2014", "AFROOIL PLC", "Buy", "Booked", 2, 20.70));
		orders.add(new Order("1/10/2014", "NOVATIA LTD", "Buy", "Complete", 4000, 1.07));
		
		orders.add(new Order("1/10/2014", "ACCESSBANK", "Buy", "Complete", 2, 26.90));
		orders.add(new Order("3/6/2014", "DIAMONDBANK PLC", "Buy", "Filled", 5000, 1.67));
		orders.add(new Order("1/9/2014", "CWG PLC", "Buy", "Booked", 2, 20.70));
		orders.add(new Order("1/10/2014", "OANDO PLC", "Buy", "Complete", 4000, 1.07));
		
		
		
		
		
		return orders;
	}
	
	public String getDate(){
		return this.date;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getBuyorSell(){
		return this.buy_or_sell;
	}
	
	public String getTradingStatus(){
		return this.trading_status;
	}
	
	public int getQuantity(){
		return this.quantity;
	}
	
	public double getUnitPrice(){
		return this.unit_price;
	}
	
	public double getValue(){
		return this.value;
	}

}
