package com.novatia.leadtrader.types;

import java.util.ArrayList;

public class News {
	
	private String title;
	private String source;
	private String date;
	
	public News(String t, String l, String date){
		this.title = t;
		this.source = l;
		this.date = date;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getSource(){
		return this.source;
	}
	
	public String getDate(){
		return this.date;
	}
	
	public static ArrayList<News> getFromRss(){
		ArrayList<News> news = new ArrayList<News>();
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"The Verge",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"Techcrunch",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"The Vangaurd",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"Punchng",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"BusinessWeek",
				"10th August"
				));
		
		return news;
	}
	
	public static ArrayList<News> getUpdatesFromRss(){
		ArrayList<News> news = new ArrayList<News>();
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		news.add(new News(
				"GTBank opens a new branch in Kenya to further their Africa network plans",
				"LeadSecurities",
				"10th August"
				));
		
		return news;
	}
}
