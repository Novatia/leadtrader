package com.novatia.leadtrader.types;

import java.util.ArrayList;

public class Stock {
	
	private String id;
	private String name;
	private String status;
	
	private int number;
	
	private double unit_price;
	private double yesterday_price;
	private double value;
	
	
	public Stock(String id, String t, String status, int number, double price){
		this.id = id;
		this.name = t;
		this.status = status;
		
		this.number = number;
			
		this.unit_price = price;
		
		this.yesterday_price = Math.round(this.unit_price - 0.21);
		
		calculateValue();
	}
	
	private void calculateValue(){
		this.value = Math.round(this.unit_price * this.number);
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getStatus(){
		return this.status;
	}
	
	public int getNumbers(){
		return this.number;
	}
	
	public double getUnitPrice(){
		return this.unit_price;
	}
	
	public double getValue(){
		return this.value;
	}
	
	public double getYesterdayPrice(){
		return this.yesterday_price;
	}
	
	public static ArrayList<Stock> getDemoCurrentStocks(){
		ArrayList<Stock> stocks = new ArrayList<Stock>();
		
		stocks.add(new Stock("7UP", "7-UP BOTTLING COMPANY", "Status", 500, 140.0));
		stocks.add(new Stock("ACCESS", "ACCESS BANK PLC", "Status", 50, 9.83));
		stocks.add(new Stock("AIICO", "7-UP BOTTLING COMPANY", "Status", 1000, 0.80));
		stocks.add(new Stock("CWG", "COMPUTER WAREHOUSE GROUP", "Status", 5000, 5.0));
		stocks.add(new Stock("CHAMS", "CHAMS PLC", "Status", 6000, 0.50));
		stocks.add(new Stock("DIAMONDBNK", "7-UP BOTTLING COMPANY", "Status", 50, 140.0));
		stocks.add(new Stock("CONOIL", "CONOIL PLC", "Status", 400, 68.0));
		stocks.add(new Stock("COSTAIN", "COSTAIN PLC", "Status", 350, 1.08));
		stocks.add(new Stock("DANGFLOUR", "DANGOTE FLOUR MILL PLC", "Status", 550, 7.80));
		stocks.add(new Stock("CUTIX", "CUTIX PLC", "Status", 670, 1.92));
		stocks.add(new Stock("CADBURY", "CADBURY NIGERIA PLC", "Status", 2200, 58.0));
		
		return stocks;
	}
	
	public static double getStockValue(ArrayList<Stock> l){
		double sum = 0.0;
		for(Stock st: l){
			sum += st.getValue();
		}
		
		return sum;
	}
	
	public static ArrayList<Stock> getNSEDemoStocks(){
ArrayList<Stock> stocks = new ArrayList<Stock>();
		
		stocks.add(new Stock("7UP", "7-UP BOTTLING COMPANY", "Status", 500, 140.0));
		stocks.add(new Stock("ACCESS", "ACCESS BANK PLC", "Status", 50, 9.83));
		stocks.add(new Stock("AIICO", "7-UP BOTTLING COMPANY", "Status", 1000, 0.80));
		stocks.add(new Stock("CWG", "COMPUTER WAREHOUSE GROUP", "Status", 5000, 5.0));
		stocks.add(new Stock("CHAMS", "CHAMS PLC", "Status", 6000, 0.50));
		stocks.add(new Stock("DIAMONDBNK", "7-UP BOTTLING COMPANY", "Status", 50, 140.0));
		stocks.add(new Stock("CONOIL", "CONOIL PLC", "Status", 400, 68.0));
		stocks.add(new Stock("COSTAIN", "COSTAIN PLC", "Status", 350, 1.08));
		stocks.add(new Stock("DANGFLOUR", "DANGOTE FLOUR MILL PLC", "Status", 550, 7.80));
		stocks.add(new Stock("CUTIX", "CUTIX PLC", "Status", 670, 1.92));
		stocks.add(new Stock("CADBURY", "CADBURY NIGERIA PLC", "Status", 2200, 58.0));
		
		stocks.add(new Stock("7UP", "7-UP BOTTLING COMPANY", "Status", 500, 140.0));
		stocks.add(new Stock("ACCESS", "ACCESS BANK PLC", "Status", 50, 9.83));
		stocks.add(new Stock("AIICO", "7-UP BOTTLING COMPANY", "Status", 1000, 0.80));
		stocks.add(new Stock("CWG", "COMPUTER WAREHOUSE GROUP", "Status", 5000, 5.0));
		stocks.add(new Stock("CHAMS", "CHAMS PLC", "Status", 6000, 0.50));
		stocks.add(new Stock("DIAMONDBNK", "7-UP BOTTLING COMPANY", "Status", 50, 140.0));
		stocks.add(new Stock("CONOIL", "CONOIL PLC", "Status", 400, 68.0));
		stocks.add(new Stock("COSTAIN", "COSTAIN PLC", "Status", 350, 1.08));
		stocks.add(new Stock("DANGFLOUR", "DANGOTE FLOUR MILL PLC", "Status", 550, 7.80));
		stocks.add(new Stock("CUTIX", "CUTIX PLC", "Status", 670, 1.92));
		stocks.add(new Stock("CADBURY", "CADBURY NIGERIA PLC", "Status", 2200, 58.0));
		
		
		return stocks;
	}
	
	
}
