package com.novatia.leadtrader.types;

import java.util.ArrayList;


public class Update {
	private String title;
	private String content;
	
	
	
	public Update(String t, String c){
		this.title = t;
		this.content = c;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getContent(){
		return this.content;
	}
	
	public static ArrayList<Update> getUpdates(){
		ArrayList<Update> updates = new ArrayList<Update>();
		for(int i=0; i < 5; i++){
			updates.add(new Update("Release of LeadTrader mobile app", "We are happy to announce the release of LeadTrader mobile app for leadsecurities limited"));
			
		}
		return updates;
		
	}
	
	
}
