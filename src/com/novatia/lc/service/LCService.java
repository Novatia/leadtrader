package com.novatia.lc.service;

import java.util.ArrayList;
import java.util.Collections;

import com.novatia.lc.api.Authentication;
import com.novatia.lc.api.BackEndService;
import com.novatia.lc.api.ClientSocket;
import com.novatia.lc.api.Controller;
import com.novatia.lc.helper.InternetHelper;
import com.novatia.lc.types.InstrunmentPrices;
import com.novatia.lc.types.Order;
import com.novatia.lc.types.Price;
import com.novatia.lc.types.ResponseCode;
import com.novatia.lc.types.Statement;
import com.novatia.lc.types.Stock;
import com.novatia.lc.types.User;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class LCService extends Service{
	IBinder mBinder = new MyBinder();
	boolean mAllowRebind;
	
	private User user;
	private ClientSocket client;
	private Controller c = new Controller();
	
	private ArrayList<Order> cachedOrders = new ArrayList<Order>();
	
	public static final double CHARGES = 1.7925;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	
	@Override
	public void onCreate(){
		
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		user = new User();
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run(){
				client = ClientSocket.createClient();
			}
		});
		
		thread.start();
		
		
		return startId;
	}
	
	@Override
	   public boolean onUnbind(Intent intent) {
	      return mAllowRebind;
	   }
	
	public class MyBinder extends Binder {
	    public LCService getService() {
	      return LCService.this;
	    }
	  }
	
	/** Called when The service is no longer used and is being destroyed */
	   @Override
	   public void onDestroy() {
//		   reset the sharedpreferences logged in data to logout
	   }
	   
	   public User getCurrentUser(){
		   return user;
	   }
	   
	   private void setUserDetails(User u){
		   if(u != null){
		   user.setAccountName(u.getAccountName());
		   user.setAccountNav(u.getAccountNav());
		   user.setCashBalance(u.getCashBalance());
		   user.setUserId(u.getUserId());
		   user.setAccountId(u.getAccountId());
		   user.setValuationDate(u.getValuationDate());
		   }
	   }
	   
	   public ArrayList<Order> getUserOrders(String startDate, String endDate){
		   
		   if(InternetHelper.isOnline()){
		   ArrayList<Order> orders = new ArrayList<Order>();
		   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
		   orders = c.getHistoricOrders(client, user.getAccountId(), startDate, endDate);		   
//		   ArrayList.sort(orders);
		   Collections.sort(orders);
		   Collections.reverse(orders);
		   
		   
		   
		   cachedOrders = orders;
		   
		   
		   
		   return orders;
		   }else{
			   
			   return cachedOrders;
			   
		   }
		   
	   }
	   
	   public ArrayList<Price> getFiveDaysPriceListOf(String instr){
		   ArrayList<Price> prices = new ArrayList<Price>();
		   if(InternetHelper.isOnline()){
			   
			   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
			   prices = c.getInstrumentPriceList(client, instr);
			   Collections.sort(prices);
//			   ArrayList.sort(orders);
//			   Collections.sort(orders);
//			   Collections.reverse(orders);
			   
			   
			   
//			   cachedOrders = orders;
			   
			   
			   
//			   return orders;
			   }else{
				   
//				   return cachedOrders;
				   
			   }
		   return prices;
		   
	   }
	   
	   public boolean addNewOrder(String instrument, String quantity, String price, String limitPrice, String side, String condition){
		   if(InternetHelper.isOnline()){
			   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
			   
			   ResponseCode co = c.addOrder(client, user.getAccountId(), instrument, quantity, limitPrice, price, side, condition);
			   
			   Log.i("ReponseCode", co.getStatus());
			   if(co.getStatus().equals(ResponseCode.success)){
//				   User tmpUser = new Controller().getUserDetails(client, user.getUserId());
//				   user.setPassword(password);
//				   setUserDetails(tmpUser);
				   login(user.getUserId(), user.getPassword());
				   return true;
			   }
			   
		   }
		   
		   return false;
	   }
	   
	   public ArrayList<Stock> getNSEData(){
		   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
		   ArrayList<Stock> nse = c.getNSEData(client, "");
		   
//		   for(Stock st: nse){
//			   st.setInstrunmentClosingPrices(c.getInstrumentPrices(client, st.getInstrumentId()));
//		   }
		   
		   return nse;
	   }
	   
	   public InstrunmentPrices getStockItemPrices(String ref){
		   InstrunmentPrices prices = new InstrunmentPrices();
		   if(InternetHelper.isOnline()){
		   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
		   
		  prices = c.getInstrumentPrices(client, ref);
		   }
		   
		   return prices;
		   
	   }
	   
	   public ArrayList<Statement> getStatements(){
		   ArrayList<Statement> statements = new ArrayList<Statement>();
		   if(InternetHelper.isOnline()){
			   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
			   statements = c.getUserAccountStatement(client, user.getAccountId());
			   
		   }else{
			   
		   }
		   
		   return statements;
		   
	   }
	   
	   
	   
	   public void setUserPortfolio(){
		   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
		   ArrayList<Stock> userStocks = c.getUserStockPortfolio(client, user.getAccountId());
		   
		   user.setPortfolio(userStocks);
	   }
	   
	   
	   public boolean changeUserPassword(String currentPassword, String newPassword){
		   Authentication auth = new Authentication(client, user.getUserId(), user.getPassword());
			   if(auth.isLoggedIn()){
				   Log.i("Service:ChangePassword login", "Successfully loggedin");
				   boolean resp = new Controller().changePassword(client, currentPassword, newPassword);
				   if(resp){
					   user.setPassword(newPassword);
					   return true;
				   }
				   
			   }
			   return false;
		   
	   }
	   
	   
	   public boolean login(String username, String password){
//		   System.out.println("In the login");
//		   XmlBuilder builder = new XmlBuilder();
//		   System.out.println("before login build");
//		   client.sendMessage(builder.buildLoginMessage(username, password));
//		   Log.i("Login response:", client.getResponse());
//		   client.sendMessage(builder.buildUserAccountDetailsMessage(username));
//		   Log.i("user data", client.getResponse());
		   
		   Authentication auth = new Authentication(client, username, password);
		   if(auth.isLoggedIn()){
			   Log.i("Service: Login", "Successfully loggedin");
			   User tmpUser = new User();
			   Log.i("Username and password:", username+":"+password);
			   tmpUser = new Controller().getUserDetails(client, username);
			   user.setPassword(password);
			   setUserDetails(tmpUser);
			   System.out.println("User in Service:"+user);
			   String cashBalance = new Controller().getUserCashBalance(client, user.getAccountId());
			   try{
			   user.setCashBalance(Double.parseDouble(cashBalance));
			   }catch(Exception e){
				   
			   }
//			   successful
			   
			   return true;
		   }else{
			   Log.i("Service: Login", "Unsuccessful");
//			   failed
		   }
		   
		   return false;
		   
	   }
	   
	   public boolean sendFeedback(String subject, String summary){
		   if(InternetHelper.isOnline()){
			   BackEndService serv = new BackEndService();
			   
			   try{
				   return serv.putFeedBack(subject, summary);
			   }catch(Exception e){
				   return false;
			   }
			   
		   }
		   
		   return false;
	   }
	   
	   public boolean isLessThanOrEqualCashBalance(Double purchaseValue){
		   
		   double margin = purchaseValue * CHARGES;
		   
		   double final_value = purchaseValue + margin;
		   
		   if(final_value < user.getCashBalance() || final_value == user.getCashBalance())
			   return true;
		   
		   
		   return false;
	   }
	   
	   public boolean sendFundRequest(String amount){
		   if(InternetHelper.isOnline()){
			   
			   BackEndService serv = new BackEndService();
			   try{
				   
				   boolean resp = serv.putFundRequest(user.getAccountId(), user.getAccountName(), amount);
				   if (resp)
					   return true;
				   
			   }catch(Exception e){
				   
			   }
			   
		   }
		   return false;
	   }
}
