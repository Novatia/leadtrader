package com.novatia.lc.helper;

import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
	
	private static final String endingZeros = "000";

	public static String today(){
		Date today = new Date();
		
		SimpleDateFormat ft = new SimpleDateFormat("yyyymmddhhmmss");
		return ft.format(today)+endingZeros;
		
	}
	
	public static String someDaysAgo(int days){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, days);
		
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH);
		
		return ft.format(cal.getTime())+endingZeros;
		
	}
	
	public static String specificDate(String date){
		return date + "240000000";
	}
	
	public static String getTifDate(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, 3);
		
		SimpleDateFormat ft = new SimpleDateFormat("yyyymmddhhmmss", Locale.ENGLISH);
		
		return ft.format(cal.getTime())+endingZeros;
	}
	
	public static void main(String args[]){
//		System.out.println(DateHelper.today());
//		System.out.println(DateHelper.specificDate("20140221"));
//		System.out.println(DateHelper.getTifDate());
		
		System.out.println(DateHelper.someDaysAgo(30));
	}
	
	
}
