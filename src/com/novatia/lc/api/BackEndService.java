package com.novatia.lc.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import java.net.HttpURLConnection;

public class BackEndService {
	
	private final String updatesUrl = "http://104.131.187.203/api/news/?output=rss";
	private final String feedbackUrl = "http://104.131.187.203/api/feedback/";
	private final String fundUrl = "http://104.131.187.203/api/fund/";
	private final String USER_AGENT = "Mozilla/5.0";
	
	public String getBackEndUpdates() throws Exception{
		
		URL obj = new URL(updatesUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		con.setRequestMethod("GET");
		 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();

 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		
		
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response.toString();
	}
	
	
	public boolean putFundRequest(String accid, String accname, String amount) throws Exception{
		URL obj = new URL(fundUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		String urlParameters = "accid="+URLEncoder.encode(accid, "UTF-8")+"&accname="+URLEncoder.encode(accname, "UTF-8")+"&amount="+URLEncoder.encode(amount, "UTF-8");
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
	
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
//		System.out.println(response.getClass());
		
		if(response.toString().equals("successful"))
			return true;
		else
			return false;
		
		
		
	}
	
	
	
	
	public boolean putFeedBack(String subject, String message) throws Exception{
		
		URL obj = new URL(feedbackUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		String urlParameters = "subject="+URLEncoder.encode(subject, "UTF-8")+"&summary="+URLEncoder.encode(message, "UTF-8");
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
	
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
//		System.out.println(response.getClass());
		
		if(response.toString().equals("successful"))
			return true;
		else
			return false;
		
//		System.out.println("Response: "+response);
		
		
	}
	
	
	public static void main(String[] args){
		BackEndService serv = new BackEndService();
//		String n = "";
		try{
//			 n = serv.getBackEndUpdates();
//			 ArrayList<News> news = new XmlParser().parseUpdates(n);
//			 System.out.println(news);
			
			System.out.println(serv.putFundRequest("3107", "Ridwan Olalere", "500000"));
			 
			 
		}catch(Exception e){
			e.printStackTrace();
		}
//		System.out.println(n);
	}

}
