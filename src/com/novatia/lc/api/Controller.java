package com.novatia.lc.api;

import java.util.ArrayList;

import android.util.Log;

import com.novatia.lc.helper.DateHelper;
import com.novatia.lc.types.InstrunmentPrices;
import com.novatia.lc.types.Order;
import com.novatia.lc.types.Price;
import com.novatia.lc.types.ResponseCode;
import com.novatia.lc.types.Statement;
import com.novatia.lc.types.Stock;
import com.novatia.lc.types.User;

public class Controller {
	private XmlBuilder builder = new XmlBuilder();
	
	
public User getUserDetails(ClientSocket client, String userId){
	client.sendMessage(builder.buildUserAccountDetailsMessage(userId));
	String response = client.getResponse();
	Log.i("response:", response);
	
	ResponseCode c = new XmlParser().parseRespondCode(response);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, response)){
			User user = new XmlParser().parseUserDetails(response);
			return user;
		}else{
//			TODO throw exception
		}
	}
	else{
//		TODO throw exception
	}
	
	return new User();
}

public ArrayList<Order> getHistoricOrders(ClientSocket client, String accountId, String startDate, String endDate){
	ArrayList<Order> hOrders = new ArrayList<Order>();
	
	client.sendMessage(builder.buildHistoricOrdersMessage(accountId, startDate, endDate));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			hOrders = new XmlParser().parseOrders(client, resp);
		}else{
//			TODO throw another exception for this
		}
		
	}else{
//		TODO throw an exception
	}
	
	
	return hOrders;
}



public String getUserCashBalance(ClientSocket client, String userId){
	String balance = "";
	
	client.sendMessage(builder.buildUserAccountCashBalanceAndValMessage(userId));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			balance = new XmlParser().parseCashBalance(resp);
			return balance;
			
		}else{
//			TODO throw exception
			System.out.println("failed in is records empty");
			System.out.println(resp);
		}
		
	}else{
//		TODO throw exception
		System.out.println("failed in getStatus");
	}	
	
	return balance;
}

public ArrayList<Stock> getUserStockPortfolio(ClientSocket client, String id){
	ArrayList<Stock> stocks = new ArrayList<Stock>();
	
	client.sendMessage(builder.buildStockValueMessage(id));
	String resp = client.getResponse();
	
//	System.out.println(resp);
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			stocks = new XmlParser().parseStockValue(resp);
			return stocks;
		}else{
//			TODO throw another kind of exception
		}
		
		
	}else{
//		TODO throw some exception based on the response code
	}
	
	
	return stocks;
}


public boolean changePassword(ClientSocket client, String currentPassword, String newPassword){
	
	client.sendMessage(builder.buildChangePasswordMessage(currentPassword, newPassword));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	if(c.getStatus().equals(ResponseCode.success)){
		
		return true;
	}
	
	return false;
}

public ArrayList<Statement> getUserAccountStatement(ClientSocket client, String id){
	ArrayList<Statement> statements = new ArrayList<Statement>();
	
	client.sendMessage(builder.buildAccountStatementMessage(id));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			
			statements = new XmlParser().parseUserAccountStatement(client, resp);
			return statements;
		}else{
//			TODO throw another exception here
		}
		
	}else{
//		TODO throw an exception
	}
	
	return statements;
}


public ArrayList<Stock> getNSEData(ClientSocket client, String stamp){
	ArrayList<Stock> todayPrices = new ArrayList<Stock>();
	
	client.sendMessage(builder.buildLatestPricesMessage(stamp));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			todayPrices = new XmlParser().parsePriceList(client, resp);
			
		}else{
//			TODO throw another exception
		}
		
	}else{
//		throw an exception
	}
	
	return todayPrices;
}


public InstrunmentPrices getInstrumentPrices(ClientSocket client, String ref){
	InstrunmentPrices price = new InstrunmentPrices();
	
	client.sendMessage(builder.buildInstrunmentMessage(ref));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			
			price = new XmlParser().parsePrices(client, resp);
		
		}else{
//			TODO throw another exception here
		}
		
	}else{
//		TODO throw an exception
	}
	
	
	return price;
}

public ArrayList<Price> getInstrumentPriceList(ClientSocket client, String instrumentId){
	ArrayList<Price> priceList = new ArrayList<Price>();
	
	client.sendMessage(builder.buildInstrumentPriceList(instrumentId));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	if(c.getStatus().equals(ResponseCode.success)){
		if(!XmlParser.isRecordsEmpty(client, resp)){
			priceList = new XmlParser().parseFiveDaysPriceList(resp);
			
		}else{
//			TODO throw exception
		}
		
	}else{
//		TODO throw exception
	}
	
	return priceList;
}


public ResponseCode addOrder(ClientSocket client, String accId, String ref, String quantity, String limitPrice, String price, String side, String condition){
	
	client.sendMessage(builder.buildNewOrderMessage(accId, ref, quantity, limitPrice, price, side, condition, DateHelper.getTifDate(), DateHelper.getTifDate()));
	String resp = client.getResponse();
	
	ResponseCode c = new XmlParser().parseRespondCode(resp);
	
	return c;
	
	
}
	

}
