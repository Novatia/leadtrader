package com.novatia.lc.api;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class GoogleNews {
	private static String path = "http://news.google.com/news?q=[q]&output=rss";
	private String url = "";
	
	private StringBuilder responseXml = new StringBuilder();
	private ArrayList newsList;
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	class ErrorCodeException extends Exception{
		
	}
	
	
	
	
	public GoogleNews(String query) throws UnsupportedEncodingException, MalformedURLException, IOException, ErrorCodeException{
		
		this.url = path.replace("[q]", URLEncoder.encode(query, "UTF-8"));
		
		URL obj = new URL(this.url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		
		if(con.getResponseCode() == 200){
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				responseXml.append(inputLine);
			}
			in.close();
			
		}else{
			System.out.println("errorcodeexception");
			throw new ErrorCodeException();
		}
		
		
	}
	
	public String getRSS(){
		return this.responseXml.toString();
	}
	
	
	
	public static void main(String[] args){
		try{
		GoogleNews news = new GoogleNews("gtbank");
		System.out.println(news.url);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}