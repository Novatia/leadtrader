package com.novatia.lc.api;

public final class SqlStatements {
	
	private static String accountStatementSql = "SELECT TOP 10 ACCID AS AccountID, ACC_NAM as ACcountName, TRANDATE as TransactionDate, UNITS as Quantity, SEC_CODE as InstrumentID, PRICE as Price, REFERENCE as ReferenceDescription, REF_NUM as ReferenceNumber, DEBIT as DebitValue, CREDIT as CreditValue, BALANCE as Balance, CSCS_ACC_NUM as CSCSNumber, EMPLOYEE as Employee, AUTHORISER as Authoriser, UNIQUEID as UniqueID, POSTDATE as PostTrade, UNIQUE_TRAN_DATE as UniqueTranDate FROM V_LC_LSILedger WHERE ACCID = '"+CustomTags.accountId+"' ORDER BY TRANDATE DESC";
	
//	stock value sql statement which is same with current holdings sql statement
	private static String stockValueSql = "SELECT UNIQUEID as UniqueID, ACCNUM as AccountID, CSCSACCNUM as CSCSNumber, " +
			"CLIENT as AccountName, QUANTITY as Quantity, PRICE as Price, INSTRUMENT as InstrumentID, " +
			"VALUE as Value FROM V_LC_StockValueReport WHERE ACCNUM = '"+CustomTags.accountId+"'";
	
//	Account details of the user
	private static String userAccountDetailsSql = "SELECT U.A_USRID AS UserID, U.A_USRNAM as UserName, A.A_ACCID as AccountID, " +
			"A.A_ACCNAM as AccountName, A.A_VALNAV as AccountNAV, A.A_VALDTE AS ValuationDate FROM T_ATH_USR" +
			" U JOIN T_ATH_USRACCLIS UAL ON U.A_USRID = UAL.A_USRID JOIN T_ATH_ACCLISMBS ALM ON UAL.A_ACCLISID = ALM.A_ACCLISID " +
			"JOIN T_ATH_ACC A ON ALM.A_ACCID = A.A_ACCID WHERE U.A_USRID = '"+CustomTags.username+"'";
	
//	DBO.CTUTCDATE() to show the present date.
//	Latest prices sql statement for the sql query request
	private static String latestPricesSql = "SELECT A_INSID AS InstrumentID, A_TKRCDE AS Ticker, A_PRC AS Price, " +
			"A_INSPRCDTE AS PriceDate FROM T_ATH_INSPRC INNER JOIN (SELECT MAX(A_REFID) AS REFID, A_INSID AS INSID " +
			"FROM T_ATH_INSPRC WHERE A_INSPRCDTE IS NOT NULL AND (A_INSPRCDTE >= LEFT('"+CustomTags.date+"', 8)) " +
			"GROUP BY A_INSID) AS IP ON (T_ATH_INSPRC.A_REFID = IP.REFID) ORDER BY A_INSID";
	
//	Account cash balance and portfolio value
	private static String accountCashBalanceAndVal = "SELECT P.A_ACCID AS AccountID, A.A_ACCNAM as AccountName, P.A_INSID as InstrumentID, " +
			"I.A_TKRCDE as TickerCode, SUM(P.A_QTY) as CashQty, SUM(P.A_VAL) as CashValue, A.A_VALNAV as AccountNAV FROM T_ATH_POSHLD P " +
			"JOIN T_ATH_ACC A ON P.A_ACCID = A.A_ACCID JOIN T_ATH_INS I ON P.A_INSID = I.A_INSID JOIN T_ATH_INSTPEMAP IM ON " +
			"I.A_INSCLSCDE1 = IM.A_INSCLSCDE WHERE IM.A_INSTPECDE = '4' AND P.A_ACCID = '"+CustomTags.accountId+"' GROUP BY P.A_ACCID, A.A_ACCNAM, P.A_INSID, " +
			"I.A_TKRCDE, A.A_VALNAV";
	
	
	private static String instrunmentDetailsSql = "SELECT A_CLOPRC, A_LSTWEKCLOPRC, A_LSTMTHCLOPRC, A_LSTQTRCLOPRC, " +
			"A_LSTYEACLOPRC  from T_ATH_INS WHERE A_INSID='"+CustomTags.instrumentId+"'";
	
	private static String instrumentPriceList = "SELECT A_INSID AS InstrumentID, A_TKRCDE AS Ticker, A_PRC AS Price, A_INSPRCDTE ASPriceDate FROM T_ATH_INSPRC IP " +
			"WHERE A_INSID = '"+CustomTags.instrumentId+"' UNION ALL SELECT A_INSID AS InstrumentID, A_TKRCDE AS Ticker, A_PRC AS Price, A_INSPRCDTE ASPriceDate FROM T_ATH_INSPRCHIS IP " +
			"WHERE A_INSID = '"+CustomTags.instrumentId+"' AND A_INSPRCDTE > LEFT(DBO.CTDateOffset(7),8) ORDER BY A_INSPRCDTE DESC";
	
	
	
	public static String getInstrunmentDetails(){
		return instrunmentDetailsSql;
	}
	
	public static String getAccountStatementSql(){
		return accountStatementSql;
	}
	
	public static String getStockValueSql(){
		return stockValueSql;
	}
	
	public static String getUserAccountDetailsSql(){
		return userAccountDetailsSql;
	}
	
	public static String getLastestPricesSql(){
		return latestPricesSql;
	}
	
	public static String getAccountCashBalanceAndValSql(){
		return accountCashBalanceAndVal;
	}
	
	public static String getInstrumentPriceListSql(){
		return instrumentPriceList;
	}
	

}
