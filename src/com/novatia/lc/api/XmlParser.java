package com.novatia.lc.api;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;

import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.novatia.lc.api.handlers.CashBalanceHandler;
import com.novatia.lc.api.handlers.ClosingPriceHandler;
import com.novatia.lc.api.handlers.RecordsCodeHandler;
import com.novatia.lc.api.handlers.ResponseCodeHandler;
import com.novatia.lc.api.handlers.RssHandler;
import com.novatia.lc.api.handlers.UpdatesHandler;
import com.novatia.lc.api.handlers.UserDetailsHandler;
import com.novatia.lc.types.InstrunmentPrices;
import com.novatia.lc.types.News;
import com.novatia.lc.types.Order;
import com.novatia.lc.types.Price;
import com.novatia.lc.types.ResponseCode;
import com.novatia.lc.types.Statement;
import com.novatia.lc.types.Stock;
import com.novatia.lc.types.User;

public class XmlParser {
	
	SAXParserFactory parserFactory;
	XmlBuilder builder = new XmlBuilder();
	
//	public ResponseCode domParseResponseCode(String message){
//		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//		try{
//			DocumentBuilder builder = factory.newDocumentBuilder();
//			Document doc = builder.parse(new InputSource(new StringReader(message)));
//			
//			NodeList list = doc.getElementsByTagName(ElementNamesAndAttributes.resultElement);
//			
//		}catch(Exception e){
//			
//		}
//	}
	
	public ResponseCode parseRespondCode(String message){
		
		 parserFactory = SAXParserFactory.newInstance();
		 
		 try{
		 SAXParser saxParser = parserFactory.newSAXParser();
		 ResponseCodeHandler handler = new ResponseCodeHandler();
		 
		 StringReader reader = new StringReader(message);
		 InputSource source = new InputSource(reader);
//		 InputStream source = new ByteArrayInputStream(message.getBytes("UTF-8"));
		 
		 saxParser.parse(source, handler);
		 
		 
		 return handler.getResponseCode();
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		return new ResponseCode();
		
	}
	
	public static boolean isRecordsEmpty(ClientSocket client, String xml){
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		int count = 0;
		
		 try{
			 SAXParser saxParser = parserFactory.newSAXParser();
			 RecordsCodeHandler handler = new RecordsCodeHandler();
			 
			 InputSource source = new InputSource(new StringReader(xml));
			 
			 saxParser.parse(source, handler);
			 
			 count = Integer.parseInt(handler.getTotalRecords());
			 if(count > 0){
				 return false;
			 }
			 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
		
		
		
		return true;
	}
	
public String parseCashBalance(String message){
	

	SAXParserFactory parserFactory = SAXParserFactory.newInstance();
	String balance = "";
	
	 try{
		 SAXParser saxParser = parserFactory.newSAXParser();
		 CashBalanceHandler handler = new CashBalanceHandler();
		 
		 InputSource source = new InputSource(new StringReader(message));
		 
		 saxParser.parse(source, handler);
		 
		 balance = handler.getCashValue();
		 
		 return balance;
		 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		
		
		
		
		return balance;
	
}
	
	
public User parseUserDetails(String message){
	
	SAXParserFactory parserFactory = SAXParserFactory.newInstance();
	User user = new User();
	
	 try{
		 SAXParser saxParser = parserFactory.newSAXParser();
		 UserDetailsHandler handler = new UserDetailsHandler();
		 
		 InputSource source = new InputSource(new StringReader(message));
		 
		 saxParser.parse(source, handler);
		 
		 user = handler.getUser();
		 
		 return user;
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
		
		
		
		
		return user;
	}

	public ArrayList<Stock> parseStockValue(String message){
		ArrayList<Stock> portfolio = new ArrayList<Stock>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try{
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(new InputSource(new StringReader(message)));
		
		NodeList recordsNode = doc.getElementsByTagName(ElementNamesAndAttributes.recordsElement);
		Node rNode = recordsNode.item(0) ;
		NodeList rr = rNode.getChildNodes();
		
		for(int i =0; i < rr.getLength(); i++){
			Stock stock = new Stock();
//			System.out.println("Counter: "+i);
			
			Node node = rr.item(i);
			
			Element element = (Element)node;
			
			stock.setAccountId(element.getAttribute(ElementNamesAndAttributes.c2));
			stock.setUniqueId(element.getAttribute(ElementNamesAndAttributes.c1));
			stock.setCsCsNumber(element.getAttribute(ElementNamesAndAttributes.c3));
			stock.setAccountName(element.getAttribute(ElementNamesAndAttributes.c4));
			stock.setQuantity(element.getAttribute(ElementNamesAndAttributes.c5));
			stock.setPrice(element.getAttribute(ElementNamesAndAttributes.c6));
			stock.setValue(element.getAttribute(ElementNamesAndAttributes.c8));
			stock.setInstrumentId(element.getAttribute(ElementNamesAndAttributes.c7));
			
			portfolio.add(stock);
			
		}
		
		
		}catch(Exception e){
//			TODO comment this out and throw an exception here
			e.printStackTrace();
		}
		
		
		
		return portfolio;
		
	}
	
	public ArrayList<Statement> parseUserAccountStatement(ClientSocket client, String message){
		ArrayList<Statement> statements = new ArrayList<Statement>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try{
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(message)));
			
			NodeList recordsNode = doc.getElementsByTagName(ElementNamesAndAttributes.recordsElement);
			Node rNode = recordsNode.item(0) ;
			NodeList rr = rNode.getChildNodes();
			
			for(int i =0; i < rr.getLength(); i++){
				Statement statement = new Statement();
//				System.out.println("Counter: "+i);
				
				Node node = rr.item(i);
				
				Element element = (Element)node;
				
				statement.setBalance(element.getAttribute(ElementNamesAndAttributes.c11));
				statement.setCreditValue(element.getAttribute(ElementNamesAndAttributes.c10));
				statement.setCsCsNumber(element.getAttribute(ElementNamesAndAttributes.c12));
				statement.setDebitValue(element.getAttribute(ElementNamesAndAttributes.c9));
				statement.setInstrumentId(element.getAttribute(ElementNamesAndAttributes.c5));
				statement.setPrice(element.getAttribute(ElementNamesAndAttributes.c6));
				statement.setQuantity(element.getAttribute(ElementNamesAndAttributes.c4));
				statement.setTransactionDate(element.getAttribute(ElementNamesAndAttributes.c3));
				statement.setReferenceDescription(element.getAttribute(ElementNamesAndAttributes.c7));
				statement.setReferenceNumber(element.getAttribute(ElementNamesAndAttributes.c8));
				statement.setPostTrader(element.getAttribute(ElementNamesAndAttributes.c16));
				
				
				statements.add(statement);
				
			}
			
			
			
		}catch(Exception e){
			
		}
		
		return statements;
	}
	
	public ArrayList<Price> parseFiveDaysPriceList(String rss){
		ArrayList<Price> prices = new ArrayList<Price>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//		c3 price and c4 date
		
		try{
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document doc = builder.parse(new InputSource(new StringReader(rss)));
			
			NodeList recordsNode = doc.getElementsByTagName(ElementNamesAndAttributes.recordsElement);
			Node rNode = recordsNode.item(0) ;
			NodeList rr = rNode.getChildNodes();
			
			for(int i = 0; i < rr.getLength(); i++){
				Price price = new Price();
				Node node = rr.item(i);
				
				Element element = (Element)node;
				
				price.setPrice(element.getAttribute(ElementNamesAndAttributes.c3));
				price.setDate(element.getAttribute(ElementNamesAndAttributes.c4));
				
				if(!prices.contains(price))
					prices.add(price);
				
			}
			
			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		
		return prices;
	}
	
	
	
	
	public ArrayList<Stock> parsePriceList(ClientSocket client, String message){
		ArrayList<Stock> priceLists = new ArrayList<Stock>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try{
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document doc = builder.parse(new InputSource(new StringReader(message)));
			
			NodeList recordsNode = doc.getElementsByTagName(ElementNamesAndAttributes.recordsElement);
			Node rNode = recordsNode.item(0) ;
			NodeList rr = rNode.getChildNodes();
			
			for(int i =0; i < rr.getLength(); i++){
				Stock st = new Stock();
//				System.out.println("Counter: "+i);
				
				Node node = rr.item(i);
				
				Element element = (Element)node;
				
				st.setInstrumentId(element.getAttribute(ElementNamesAndAttributes.c1));
				st.setPrice(element.getAttribute(ElementNamesAndAttributes.c3));
				
				priceLists.add(st);
				
			}
			
			
			
		}catch(Exception e){
			
		}
		
		
		return priceLists;
	}
	
	public ArrayList<Order> parseOrders(ClientSocket client, String xml){
		ArrayList<Order> orders = new ArrayList<Order>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try{
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(new InputSource(new StringReader(xml)));
		
		NodeList recordsNode = doc.getElementsByTagName(ElementNamesAndAttributes.recordsElement);
		Node rNode = recordsNode.item(0) ;
		NodeList rr = rNode.getChildNodes();
		
		for(int i =0; i < rr.getLength(); i++){
			Order order = new Order();
//			System.out.println("Counter: "+i);
			
			Node node = rr.item(i);
			
			Element element = (Element)node;
			
			order.setRef(element.getAttribute(ElementNamesAndAttributes.c1));
			order.setInstrumentId(element.getAttribute(ElementNamesAndAttributes.c28));
			order.setPrice(element.getAttribute(ElementNamesAndAttributes.c34));
			order.setQuantity(element.getAttribute(ElementNamesAndAttributes.c39));
			order.setValue(element.getAttribute(ElementNamesAndAttributes.c40));
			order.setSide(element.getAttribute(ElementNamesAndAttributes.c23));
			order.setTradingStatus(element.getAttribute(ElementNamesAndAttributes.c7));
			order.setTransactionDate(element.getAttribute(ElementNamesAndAttributes.c8));
			
			orders.add(order);
			
		}
		
		}catch(Exception e){
			
		}
		
		return orders;
	}

	public InstrunmentPrices parsePrices(ClientSocket client, String message){
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		InstrunmentPrices instr = new InstrunmentPrices();
		
		 try{
			 SAXParser saxParser = parserFactory.newSAXParser();
			 ClosingPriceHandler handler = new ClosingPriceHandler();
			 
			 InputSource source = new InputSource(new StringReader(message));
			 
			 saxParser.parse(source, handler);
			 
			 instr = handler.getPrices();
			 
			 
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			
			
			
			
			
			return instr;

	}
	
	public ArrayList<News> parseNews(String rss){
		ArrayList<News> news = new ArrayList<News>();
		
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		
		try{
			
			SAXParser parser = parserFactory.newSAXParser();
			RssHandler rssHandler = new RssHandler();
			
			InputSource source = new InputSource(new StringReader(rss));
			 
			parser.parse(source, rssHandler);
			
			news = rssHandler.getNewsList();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return news;
		
	}
	
	
	public ArrayList<News> parseUpdates(String rss){
		ArrayList<News> news = new ArrayList<News>();
		
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		
		try{
			SAXParser parser = parserFactory.newSAXParser();
			UpdatesHandler handler = new UpdatesHandler();
			
			InputSource source = new InputSource(new StringReader(rss));
			parser.parse(source, handler);
			
			news = handler.getNewsList();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return news;
		
	}
}
