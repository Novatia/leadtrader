package com.novatia.lc.api;

import java.io.BufferedInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Date;

public final class ClientSocket {
	final private String ipAddress = "74.122.120.14";
	
//	74.122.120.14
//	41.76.193.15
	
	final private int portNumber = 21999;
	
	private String lastMessage = "";
	private String lastResponse = "";
	
	private Socket client;
	private PrintStream _out;
	private BufferedInputStream in;
	
	private StringBuilder strBuilder = new StringBuilder();
	
	private String endOfResponse = "</AthenaResponse>";
	
	
	
	private ClientSocket(){
		try{
			
		client = new Socket(ipAddress, portNumber);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private void setLastResponse(String response){
		this.lastResponse = response.trim();
		System.out.println(this.lastResponse);
	}
	
	public static ClientSocket createClient(){
		return new ClientSocket();
	}
	
	public void sendMessage(String message){
		this.lastMessage = message;
		try{
			
			System.out.println(this.lastMessage);
			_out = new PrintStream(client.getOutputStream());
			_out.print(this.lastMessage);

			
			
			
			byte[] buffer = new byte[10];
			int bytesRead = 0;
			
			long startTime = System.currentTimeMillis();
			in = new BufferedInputStream(client.getInputStream());
			while((bytesRead = in.read(buffer)) != -1){

				
//				System.out.println("fetching...");
				String str = new String(buffer, 0, bytesRead);
				strBuilder.append(str);
				
				String tmpString = strBuilder.toString();
				
				if(tmpString.contains(endOfResponse)){

					break;
				}
			
				
			}
			
			long endTime = (new Date()).getTime();		
			long interval = endTime - startTime;
			System.out.println("Time taken to respond: "+interval+" milli seconds ");
			System.out.println(strBuilder.toString());
			setLastResponse("<?xml version='1.0' encoding='UTF-8'?>"+strBuilder.toString());
			strBuilder = new StringBuilder();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public String getResponse(){
		return this.lastResponse;
	}
}
