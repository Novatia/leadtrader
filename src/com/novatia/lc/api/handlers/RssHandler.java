package com.novatia.lc.api.handlers;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.types.News;

public class RssHandler extends DefaultHandler {
	
	private ArrayList<News> newsList = new ArrayList<News>();
	private String content = null;
	private News news_item = null;
	
	public ArrayList<News> getNewsList(){
		return newsList;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException{
		if(qName.equals("item")){
			news_item = new News();
		}
		
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException{
		
		if(qName.equals("item")){
			newsList.add(news_item);
		}else if(qName.equals("title")){
			if(news_item != null)
			news_item.setTitle(content);
			
		}else if(qName.equals("link")){
			if(news_item != null)
			news_item.setLink(content);
			
		}else if(qName.equals("pubDate")){
			if(news_item != null)
			news_item.setDate(content);
		}else{
			
		}
		
	}
	
	@Override
	public void characters(char[] ch, int start, int len){
		
		content = new String(ch, start, len);
		
	}
	
	
}
