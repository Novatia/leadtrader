package com.novatia.lc.api.handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.api.ElementNamesAndAttributes;

public class RecordsCodeHandler extends DefaultHandler {
	
	private String totalRecords = "0";
	
	public String getTotalRecords(){
		return this.totalRecords;
	}
	
	@Override
	public void startDocument() throws SAXException{
		
	}
	
	@Override
	public void endDocument() throws SAXException{
		
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
		if(qName.equals(ElementNamesAndAttributes.recordsElement)){
			totalRecords = attributes.getValue(ElementNamesAndAttributes.countAttribute);
//			System.out.println(totalRecords);
			
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName){
		
	}
	
	@Override
	public void characters(char ch[], int start, int end) throws SAXException{
		
	}

}
