package com.novatia.lc.api.handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.api.ElementNamesAndAttributes;
import com.novatia.lc.types.ResponseCode;

public class ResponseCodeHandler extends DefaultHandler{
	private ResponseCode resp = new ResponseCode();
	
	public ResponseCode getResponseCode(){
		return this.resp;
	}
	
	@Override
	public void startDocument() throws SAXException{
		
	}
	
	@Override
	public void endDocument() throws SAXException{
		
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
		if(qName.equals(ElementNamesAndAttributes.resultElement)){
//			System.out.println("Got into to startElement: status Attribute:"+attributes.getValue(ElementNamesAndAttributes.codeAttribute));
			resp.setStatus(attributes.getValue(ElementNamesAndAttributes.codeAttribute));
			resp.setMessage(attributes.getValue(ElementNamesAndAttributes.descriptionAttribute));
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException{
		
	}
	
	@Override
	public void characters(char ch[], int start, int end) throws SAXException{
		
	}
	
}
