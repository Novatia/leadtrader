package com.novatia.lc.api.handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.api.ElementNamesAndAttributes;
import com.novatia.lc.types.InstrunmentPrices;

public class ClosingPriceHandler extends DefaultHandler{
	
	private InstrunmentPrices prices = new InstrunmentPrices();
	
	public InstrunmentPrices getPrices(){
		return prices;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException{
		if(qName.equals(ElementNamesAndAttributes.r1)){
			prices.setClosePrice(attr.getValue(ElementNamesAndAttributes.c1));
			prices.setLastWeekClosePrice(attr.getValue(ElementNamesAndAttributes.c2));
			prices.setLastMonthClosePrice(attr.getValue(ElementNamesAndAttributes.c3));
			prices.setLastQuarterClosePrice(attr.getValue(ElementNamesAndAttributes.c4));
			prices.setLastYearClosePrice(attr.getValue(ElementNamesAndAttributes.c5));
		}
		
	}
	
	

}
