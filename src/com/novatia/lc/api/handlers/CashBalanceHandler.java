package com.novatia.lc.api.handlers;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.api.ElementNamesAndAttributes;

public class CashBalanceHandler extends DefaultHandler {
	
	private String cashValue;
	
	public String getCashValue(){
		return this.cashValue;
	}
	
	@Override
	public void startDocument(){
		
	}
	
	@Override
	public void endDocument(){
		
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr){
		if(qName.equals(ElementNamesAndAttributes.r1)){
			cashValue = attr.getValue(ElementNamesAndAttributes.c6);
			System.out.println(cashValue);
		}
		
	}
	
	@Override
	public void endElement(String uri, String localName, String qName){
		
	}

}
