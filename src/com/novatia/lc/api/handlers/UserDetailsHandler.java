package com.novatia.lc.api.handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.novatia.lc.api.ElementNamesAndAttributes;
import com.novatia.lc.types.User;


public class UserDetailsHandler extends DefaultHandler {
	private User user = new User();
	
	public User getUser(){
		return this.user;
	}
	
	@Override
	public void startDocument() throws SAXException{
		
	}
	
	@Override
	public void endDocument() throws SAXException{
		
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
		if(qName.equals(ElementNamesAndAttributes.r1)){
			user.setUserId(attributes.getValue(ElementNamesAndAttributes.c1));
			user.setAccountId(attributes.getValue(ElementNamesAndAttributes.c3));
			user.setUserName(attributes.getValue(ElementNamesAndAttributes.c2));
			user.setAccountName(attributes.getValue(ElementNamesAndAttributes.c4));
			user.setAccountNav(Double.parseDouble(attributes.getValue(ElementNamesAndAttributes.c5)));
			user.setValuationDate(attributes.getValue(ElementNamesAndAttributes.c6));
//			user.setCashBalance(Double.parseDouble(attributes.getValue(ElementNamesAndAttributes.)));
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName){
		
	}
	
	@Override
	public void characters(char ch[], int start, int end) throws SAXException{
		
	}

}
