package com.novatia.lc.api;

public class CustomTags {
	
	public final static String username = "[username]";
	
	public final static String password = "[password]";
	
	public final static String userid = "[userid]";
	
	public final static String accountId = "[accountid]";
	
	public final static String startDate = "[startdate]";
	
	public final static String endDate = "[enddate]";
	
	public final static String date = "[date]";
	
	public final static String instrumentId = "[instrunment]";
	
	public final static String instrumentRef = "[instrumentRef]";
	
	public final static String quantity = "[quantity]";
	
	public final static String condition = "[condition]";
	
	public final static String limitPrice = "[limitprice]";
	
	public final static String price = "[price]";
	
	public final static String side = "[side]";
	
	public final static String settleDate = "[settledate]";
	
	public final static String tifDate = "[tifdate]";
	
	public final static String currentPassword = "[currentpassword]";
	
	public final static String newPassword = "[newpassword]";
	
	public final static String confirmPassword = "[confirmpassword]";

}
