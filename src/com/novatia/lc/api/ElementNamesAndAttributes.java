package com.novatia.lc.api;

public class ElementNamesAndAttributes {
	
	public final static String resultElement = "Result";
	public final static String codeAttribute = "code";
	public final static String descriptionAttribute = "description";
	
	public final static String recordsElement = "Records";
	public final static String countAttribute = "count";
	
	public final static String r1 = "r1";
	
	
	public final static String c1 = "c1";
	public final static String c2 = "c2";
	public final static String c3 = "c3";
	public final static String c4 = "c4";
	public final static String c5 = "c5";
	public final static String c6 = "c6";
	public final static String c7 = "c7";
	public final static String c8 = "c8";
	public final static String c9 = "c9";
	public final static String c10 = "c10";
	public final static String c11 = "c11";
	public final static String c12 = "c12";
	public final static String c13 = "c13";
	public final static String c14 = "c14";
	public final static String c15 = "c15";
	public final static String c16 = "c16";
	public final static String c17 = "c17";
	public final static String c18 = "c18";
	
	public final static String c23 = "c23";
	public final static String c28 = "c28";
	public final static String c39 = "c39";
	public final static String c40 = "c40";
	public final static String c34 = "c34";
	
	

}
