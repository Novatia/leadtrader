package com.novatia.lc.api;

import com.novatia.lc.helper.DateHelper;

public final class XmlBuilder {
	
//	user xml request message for user to login the clientsocket
	private String loginXml = "<AthenaRequest reference=\"1\">" +
			"<Login userId=\""+CustomTags.username+"\" password=\""+CustomTags.password+"\" />" +
			"</AthenaRequest>";
	
	private String changePasswordXml = "<AthenaRequest reference='8'>" +
			"<ChangePassword currentPassword='"+CustomTags.currentPassword+"' " +
			"newPassword='"+CustomTags.newPassword+"' confirmNewPassword='"+CustomTags.confirmPassword+"'/>" +
			"</AthenaRequest>";
	
	private String newOrderXml = "<AthenaRequest reference='1'>" +
			"<AddOrders>" +
			"<Order accountId='"+CustomTags.accountId+"' instrumentRef='"+CustomTags.instrumentRef+"' instrumentRefType='ID' extOrderId=''" +
			" tif='C' tifDate='"+CustomTags.tifDate+"' tradeNote='Order created via xml API'" +
			" settleCurrency='NGN' unitType='Q'  quantity='"+CustomTags.quantity+"' side='"+CustomTags.side+"' " +
			"price='"+CustomTags.price+"' condition='"+CustomTags.condition+"' limitPrice='"+CustomTags.limitPrice+"' " +
					"settleDate='"+CustomTags.settleDate+"'/>" +
			"</AddOrders>" +
			"</AthenaRequest>"; 
	
//	request xml message for history order and new orders together. Just change the date range to switch
	private String historicOrderXml = "<AthenaRequest reference='1'>" +
			"<Historicorders accountid='"+CustomTags.accountId+"' startDate='"+CustomTags.startDate+"' endDate='"+CustomTags.endDate+"'/>" +
			"</AthenaRequest>";
	
	
	private String latestPrices = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getLastestPricesSql()+"\"/>" +
					"</AthenaRequest>";
	
	private String instrumentPriceList = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getInstrumentPriceListSql()+"\"/>" +
					"</AthenaRequest>";
	
	
//	statement of account request message
	private String accountStatement = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getAccountStatementSql()+"\" />" +
					"</AthenaRequest>";
	
//	xml request message for user account details
	private String userAccountDetails = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getUserAccountDetailsSql()+"\"/>" +
					"</AthenaRequest>";
	
//	xml request message for user cash value
	private String userCashBalanceAndVal = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getAccountCashBalanceAndValSql()+"\"/>" +
					"</AthenaRequest>";
	
	
//	xml request message for users stock items just like current holdings
	private String stockValue = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getStockValueSql()+"\"/>" +
					"</AthenaRequest>";
	
	
	private String instrunmentXml = "<AthenaRequest reference='2'>" +
			"<SQL query=\""+SqlStatements.getInstrunmentDetails()+"\"/>" +
					"</AthenaRequest>";
	
	
	public String buildInstrunmentMessage(String instrunmentId){
		String tmpXml = instrunmentXml;
		
		tmpXml = tmpXml.replace(CustomTags.instrumentId, instrunmentId);
		
		return tmpXml;
	}
	
	
	
	public String buildLoginMessage(String username, String password){
		String tmpXml = this.loginXml;
		
		tmpXml = tmpXml.replace(CustomTags.username, username);
		tmpXml = tmpXml.replace(CustomTags.password, password);
		
		
		return tmpXml;
	}
	
	public String buildUserAccountDetailsMessage(String username){
		String tmpXml = userAccountDetails;
		
		tmpXml = tmpXml.replace(CustomTags.username, username);
		
		return tmpXml;
	}
	
	public String buildAccountStatementMessage(String accountId){
		
		String tmpXml = accountStatement;
		tmpXml = tmpXml.replace(CustomTags.accountId, accountId);
		
		return tmpXml;
	}
	
	public String buildLatestPricesMessage(String date){
		String tmpXml = latestPrices; 
		if (date.equals("")){
			tmpXml = tmpXml.replace(CustomTags.date, DateHelper.someDaysAgo(0));
		}else{
			tmpXml = tmpXml.replace(CustomTags.date, "DBO.CTUTCDATE()");
		}
		return tmpXml;
	}
	
	public String buildNewOrderMessage(String accountId, String instrumentRef, String quantity, String limitPrice, String price, String side, String condition, String settleDate, String tifDate){
		String tmpXml = newOrderXml;
		
		tmpXml = tmpXml.replace(CustomTags.tifDate, tifDate);
		tmpXml = tmpXml.replace(CustomTags.accountId, accountId);
		tmpXml = tmpXml.replace(CustomTags.instrumentRef, instrumentRef);
		tmpXml = tmpXml.replace(CustomTags.quantity, quantity);
		tmpXml = tmpXml.replace(CustomTags.limitPrice, limitPrice);
		tmpXml = tmpXml.replace(CustomTags.price, price);
		tmpXml = tmpXml.replace(CustomTags.side, side);
		tmpXml = tmpXml.replace(CustomTags.condition, condition);
		tmpXml = tmpXml.replace(CustomTags.settleDate, settleDate);
		
		return tmpXml;
	}
	
	public String buildUserAccountCashBalanceAndValMessage(String accountId){
		String tmpXml = userCashBalanceAndVal;
		
		tmpXml = tmpXml.replace(CustomTags.accountId, accountId);
		
		return tmpXml;
	}
	
	public String buildStockValueMessage(String accountId){
		String tmpXml = stockValue;
		
		tmpXml = tmpXml.replace(CustomTags.accountId, accountId);
		
		return tmpXml;
	}
	
	
	public String buildHistoricOrdersMessage(String accountId, String startDate, String endDate){
		String tmpXml = historicOrderXml;
		
		tmpXml = tmpXml.replace(CustomTags.accountId, accountId);
		tmpXml = tmpXml.replace(CustomTags.startDate, startDate);
		tmpXml = tmpXml.replace(CustomTags.endDate, endDate);
		
		return tmpXml;
		
	}
	
	
	public String buildChangePasswordMessage(String currentPassword, String newPassword){
		String tmpXml = changePasswordXml;
		
		tmpXml = tmpXml.replace(CustomTags.currentPassword, currentPassword);
		tmpXml = tmpXml.replace(CustomTags.newPassword, newPassword);
		tmpXml = tmpXml.replace(CustomTags.confirmPassword, newPassword);
		
		return tmpXml;
		
	}
	
	public String buildInstrumentPriceList(String instrumentId){
		String tmpXml = instrumentPriceList;
		
		tmpXml = tmpXml.replace(CustomTags.instrumentId, instrumentId);
		tmpXml = tmpXml.replace(CustomTags.instrumentId, instrumentId);
		
		return tmpXml;
	}
	
	
	
	public static void main(String args[]){
		System.out.println(new XmlBuilder().buildInstrunmentMessage("AFROIL"));
	}
	

}
