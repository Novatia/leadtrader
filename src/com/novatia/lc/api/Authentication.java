package com.novatia.lc.api;

import com.novatia.lc.types.ResponseCode;
import com.novatia.lc.types.User;

public class Authentication {
	
	private User user;
	
	private ResponseCode resp;
	
	private String failedLoginReason;
	
	private boolean loggedIn = false;

	
	private XmlBuilder builder;
	
	public String getFailedLoginReason(){
		return this.failedLoginReason;
	}
	
	public boolean isLoggedIn(){
		return this.loggedIn;
	}
	
	public Authentication(ClientSocket client, String username, String password){
		builder = new XmlBuilder();
		
		client = ClientSocket.createClient();
		client.sendMessage(builder.buildLoginMessage(username, password));
		
		String response = client.getResponse();
		
		XmlParser parser = new XmlParser();
		ResponseCode res = parser.parseRespondCode(response);
		
		if(res.getStatus().equals(ResponseCode.success)){
			loggedIn = true;
			
		}
		else{
			failedLoginReason = res.getMessage();
			
		}
		
	}
	

}
