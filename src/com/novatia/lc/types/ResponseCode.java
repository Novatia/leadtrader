package com.novatia.lc.types;

public class ResponseCode {
	
	public static String success = "0";
	public static String sessionNotLoggedIn = "-1";
	public static String scheduleError = "-10";
	public static String SystemError = "-100";
	
	
	private String status;
	private String message;
	
	public void setStatus(String s){
		this.status = s;
	}
	
	public void setMessage(String m){
		this.message = m;
	}
	
	public String getStatus(){
		return this.status;
	}
	
	public String getMessage(){
		return this.message;
	}

}
