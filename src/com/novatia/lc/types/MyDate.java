package com.novatia.lc.types;

import java.util.Calendar;
import java.util.Date;

public class MyDate {
	
	private String year;
	private String month;
	private String day;
	
	
	public void setYear(String yr){
		this.year = yr;
	}
	
	public void setMonth(String mn){
		int m = Integer.parseInt(mn);
		m += 1;
		mn = Integer.toString(m);
		if(mn.length() < 2){
			this.month = "0"+mn;
		}else{
			this.month = mn;
		}
		
	}
	
	public void setDay(String d){
		if(d.length() < 2){
			this.day = "0"+d;
		}else{
			this.day = d;
		}
	}
	
	public Date getDateObj(){
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(this.year), Integer.parseInt(this.month)-1, Integer.parseInt(this.day));
		return c.getTime();
	}
	
	
	public String getYear(){
		return this.year;
	}
	
	public String getDay(){
		return this.day;
	}
	
	public String getMonth(){
		return this.month;
	}
	
	public String date(){
		return this.year+this.month+this.day;
	}

}
