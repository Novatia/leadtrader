package com.novatia.lc.types;

import com.novatia.lc.helper.DecimalPlaces;

public class Statement {
	
	private String creditValue;
	private String balance;
	private String uniqueId;
	private String price;
	private String referenceDescription;
	private String referenceNumber;
	private String debitValue;
	private String transactionDate;
	private String quantity;
	private String instrumentId;
	private String csCsNumber;
	private String postTrade;
	
	public void setPostTrader(String p){
		this.postTrade = p.trim();
	}
	
	public void setCreditValue(String c){
		this.creditValue = c.trim();
	}
	
	public void setBalance(String b){
		this.balance = b.trim();
	}
	
	public void setUniqueId(String id){
		this.uniqueId = id.trim();
	}
	
	public void setPrice(String price){
		this.price = price.trim();
	}

	public void setReferenceDescription(String desc){
		this.referenceDescription = desc.trim();
	}
	
	public void setReferenceNumber(String n){
		this.referenceNumber = n.trim();
	}
	
	public void setDebitValue(String v){
		this.debitValue = v.trim();
	}
	
	public void setTransactionDate(String d){
		this.transactionDate = d.trim();
	}
	
	public void setQuantity(String q){
		this.quantity = q.trim();
	}
	
	public void setInstrumentId(String id){
		this.instrumentId = id.trim();
	}
	
	public void setCsCsNumber(String num){
		this.csCsNumber = num.trim();
	}
	
	public String getCreditValue(){
		if(this.creditValue.equals("") | this.price.equals(" "))
			return this.creditValue;
		else
			return Double.toString(DecimalPlaces.round(Double.parseDouble(this.creditValue), 2));
	}
	
	public String getPrice(){
		if(this.price.equals("") | this.price.equals(" "))
			return this.price;
		else
			return Double.toString(DecimalPlaces.round(Double.parseDouble(this.price), 2));
	}
	
	public String getUniqueId(){
		return this.uniqueId;
	}
	
	public String getQuantity(){
		return this.quantity;
	}
	
	public String getDebitValue(){
		if(this.debitValue.equals("") | this.debitValue.equals(" "))
			return this.price;
		else
			return Double.toString(DecimalPlaces.round(Double.parseDouble(this.debitValue), 2));
	}
	
	public String getBalance(){
		
		return this.balance;
//		return Double.toString(DecimalPlaces.round(Double.parseDouble(this.balance), 2));
	}
	
	public String getInstrumentId(){
		return this.instrumentId;
	}
	
	public String getReferenceDescription(){
		return this.referenceDescription;
	}
	
	public String getReferenceNumber(){
		return this.referenceNumber;
	}
	
	public String getTransactionDate(){
		return this.transactionDate;
	}
	
	public String getPostTrader(){
		return this.postTrade;
	}
	
	@Override
	public String toString(){
		return this.transactionDate;
	}
}
