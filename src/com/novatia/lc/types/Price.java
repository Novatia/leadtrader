package com.novatia.lc.types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Price implements Comparable<Price> {
	
	private double price;
	private Date date;
	
	
	public void setPrice(double price){
		this.price = price;
	}
	
	public void setPrice(String price){
		this.price = Double.parseDouble(price);
	}
	
	public void setPrice(int price){
		this.price = price;
	}
	
	public void setDate(String d){
		try {
			this.date = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH).parse(d.substring(0, 14));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public double getPrice(){
		return this.price;
	}
	
	public Date getDate(){
		return this.date;
	}
	
	@Override
	public int hashCode(){
		return this.date.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		Price p = (Price)o;
			
		if(this.date.getDay() == p.getDate().getDay()){
			return true;
		}else{
			return false;
		}		
	}
	

	@Override
	public int compareTo(Price o) {
		// TODO Auto-generated method stub
		return this.date.compareTo(o.getDate());
	}
	
	@Override
	public String toString(){
		return Double.toString(this.price) + " : " + this.date;
	}
	
	
}
