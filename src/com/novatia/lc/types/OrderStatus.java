package com.novatia.lc.types;

public class OrderStatus {
	
	private final static String placed = "1";
	private final static String unplaced = "0";
	private final static String partFillPlaced = "2";
	private final static String partialFill = "3";
	private final static String filled = "4";
	private final static String booked = "5";
	private final static String complete = "6";
	
	public static String getStatus(String code){
		String status = "";
		
		if(code.equals(placed)){
			status = "Placed";
			
		}else if(code.equals(unplaced)){
			status = "Unplaced";
			
		}else if(code.equals(partFillPlaced)){
			status = "Part Filled";
			
		}else if(code.equals(partialFill)){
			
			status = "Partial Filled";
			
		}else if(code.equals(filled)){
			
			status = "Filled";
			
		}else if(code.equals(booked)){
			
			status = "Booked";
			
		}else if(code.equals(complete)){
			
			status = "Complete";
			
		}else{
			
		}
		
		return status;
		
		
	}

}
