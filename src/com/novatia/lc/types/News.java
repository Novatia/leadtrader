package com.novatia.lc.types;

public class News {
	
	private String title = "";
	private String summary = "";
	private String source = "";
	private String link = "";
	private String date = "";
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setSummary(String summary){
		this.summary = summary;
	}
	
	public void setSource(String source){
		this.source = source;
	}
	
	public void setLink(String l){
		if(l.contains("=")){
		String[] b = l.split("=");
		this.link = b[1];
		}else{
			this.link = l;
		}
		
	}
	
	public void setDate(String d){
		this.date = d;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getSummary(){
		return this.summary;
	}
	
	public String getSource(){
		return this.source;
	}
	
	public String getLink(){
		return this.link;
	}
	
	public String getDate(){
		return this.date;
	}
	
	
	@Override
	public String toString(){
		return "("+this.getTitle()+", "+this.getLink()+", "+this.getDate()+")";
	}

}
