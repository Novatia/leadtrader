package com.novatia.lc.types;

public class InstrunmentPrices {
	
	private String lastWeekClosePrice;
	private String closePrice;
	private String lastMonthClosePrice;
	private String lastQuarterClosePrice;
	private String lastYearClosePrice;
	
	public void setLastWeekClosePrice(String p){
		this.lastWeekClosePrice = p;
	}

	public void setClosePrice(String p){
		this.closePrice = p;
	}
	
	public void setLastMonthClosePrice(String p){
		this.lastMonthClosePrice = p;
	}
	
	public void setLastQuarterClosePrice(String p){
		this.lastQuarterClosePrice = p;
	}
	
	public void setLastYearClosePrice(String p){
		this.lastYearClosePrice = p;
	}
	
	public String getLastWeekClosePrice(){
		return this.lastWeekClosePrice;
	}
	
	public String getLastMonthClosePrice(){
		return this.lastMonthClosePrice;
	}
	
	public String getClosePrice(){
		return this.closePrice;
	}
	
	public String getLastQuarterClosePrice(){
		return this.lastQuarterClosePrice;
	}
	
	public String getLastYearClosePrice(){
		return this.lastYearClosePrice;
	}
	
	@Override
	public String toString(){
		return this.closePrice;
	}
	
}
