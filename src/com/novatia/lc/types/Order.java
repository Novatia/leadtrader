package com.novatia.lc.types;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Order implements Comparable<Order> {
	
	private String instrumentId;
	private String price;
	private String value;
	private String transactionDate;
	private String tradingStatus;
	private String side;
	private String quantity;
	private String ref;
	
	public void setRef(String ref){
		this.ref = ref;
	}
	
	public void setInstrumentId(String id){
		this.instrumentId = id;
	}
	
	public void setPrice(String p){
		this.price = p;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public void setTransactionDate(String date){
		this.transactionDate = date;
	}
	
	public void setTradingStatus(String status){
		this.tradingStatus = status;
	}
	
	public void setSide(String side){
		this.side = side;
	}
	
	public void setQuantity(String q){
		this.quantity = q;
	}
	
	public String getQuantity(){
		return this.quantity;
	}
	
	public String getSide(){
		return this.side;
	}
	
	public String getTradingStatus(){
		return this.tradingStatus;
	}
	
	public String getTransactionDate(){
		return this.transactionDate;
	}
	
	public String getInstrumentId(){
		return this.instrumentId;
	}
	
	public String getPrice(){
		return this.price;
	}
	
	public String getRef(){
		return this.ref;
	}
	
	public String getValue(){
		return this.value;
	}
	
	@Override
	public String toString(){
		return this.instrumentId+":"+OrderStatus.getStatus(this.tradingStatus)+":"+this.transactionDate;
	}
	
	public Date getDateObject(){
//		String string = "January 2, 2010";
		Date date = new Date();
		try{
		date = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH).parse(this.transactionDate.substring(0, 14));
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return date;
		}
		
	}
	
	@Override
	public int compareTo(Order o){
//		Order oo = (Order)o;
		return this.getDateObject().compareTo(o.getDateObject());
	}

}
