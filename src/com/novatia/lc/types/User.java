package com.novatia.lc.types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.novatia.lc.helper.DecimalPlaces;

public class User {
	
	private String userId;
	private String userName;
	private String accountId;
	private String accountName;
	private double accountNav;
	private double cashBalance;
	private Date valuationDate;
	
	private ArrayList<Stock> portfolio;
	private ArrayList<Statement> myStatement;
	private ArrayList<Order> myOrders;
	
	private String password;
	
	
	public void setPassword(String p){
		password = p;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public void setUserId(String id){
		this.userId = id;
	}
	
	public void setCashBalance(double b){
		this.cashBalance = b;
	}
	
	public void setUserName(String name){
		this.userName = name;
	}
	
	public void setAccountId(String id){
		this.accountId = id;
	}
	
	public void setAccountName(String name){
		this.accountName = name;
	}
	
	public void setAccountNav(double nav){
		this.accountNav = nav;
	}
	
	public void setValuationDate(String date){
		try {
			this.valuationDate = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH).parse(date.substring(0, 14));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setValuationDate(Date date){
		this.valuationDate = date;
	}
	
	public Date getValuationDate(){
		return this.valuationDate;
	}
	
	
	public String getUserId(){
		return this.userId;
	}
	
	public String getUserName(){
		return this.userName;
	}
	
	public String getAccountId(){
		return this.accountId;
	}
	
	public String getAccountName(){
		return this.accountName;
	}
	
	public double getAccountNav(){
		
		
		return DecimalPlaces.round(this.accountNav, 2);
	}
	
	public double getCashBalance(){
		return DecimalPlaces.round(this.cashBalance, 2);
	}
	
	public void setPortfolio(ArrayList<Stock> t){
		this.portfolio = t;
	}
	
	public ArrayList<Stock> getPorfolio(){
		return this.portfolio;
	}
	
	@Override
	public String toString(){
		return this.accountId;
	}

}
