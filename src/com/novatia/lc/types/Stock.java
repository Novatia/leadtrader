package com.novatia.lc.types;

import com.novatia.lc.helper.DecimalPlaces;

public class Stock {
	
	private String uniqueId;
	private String accountId;
	private String csCsNumber;
	private String accountName;
	private String quantity;
	private String instrumentId;
	private String price;
	private String value;
	private String priceDate;
	private String tradingStatus;
	
	private InstrunmentPrices closingPrices;
	
	public void setTradingStatus(String status){
		this.tradingStatus = status;
	}
	
	public void setInstrunmentClosingPrices(InstrunmentPrices p){
		this.closingPrices = p;
	}
	
	public void setPriceDate(String date){
		this.priceDate = date;
	}
	
	public InstrunmentPrices getClosingPrices(){
		return this.closingPrices;
	}
	
	public String getUniqueId(){
		return uniqueId;
	}
	
	public String getAccountId(){
		return accountId;
	}
	
	public String getCsCsNumber(){
		return csCsNumber;
	}
	
	public String getAccountName(){
		return accountName;
	}
	
	public String getQuantity(){
		return quantity;
	}
	
	public String getPrice(){
//		System.out.println();
		this.price = Double.toString(DecimalPlaces.round(Double.parseDouble(this.price), 2));
		
//		double parsed = Double.parseDouble(this.price);
//		this.price = NumberFormat.getCurrencyInstance().format((parsed/100));
		
		return this.price;
	}
	
	public String getInstrumentId(){
		return instrumentId;
	}
	
	public String getValue(){
		this.value = Double.toString(DecimalPlaces.round(Double.parseDouble(this.value), 2));
		
//		double parsed = Double.parseDouble(this.value);
//		this.value = NumberFormat.getCurrencyInstance().format((parsed/100));
		
		return value;
	}
	
	
	
	public void setUniqueId(String id){
		this.uniqueId = id.trim();
	}
	
	public void setAccountId(String id){
		this.accountId = id.trim();
	}
	
	public void setCsCsNumber(String num){
		this.csCsNumber = num.trim();
	}
	
	public void setQuantity(String q){
		this.quantity = q.trim();
	}
	
	public void setAccountName(String n){
		this.accountName = n.trim();
	}
	
	public void setPrice(String p){
		this.price = p.trim();
	}
	
	public void setInstrumentId(String id){
		this.instrumentId = id.trim();
	}
	
	public void setValue(String v){
		this.value = v.trim();
	}
	
	@Override
	public String toString(){
		return this.instrumentId;
	}
	
	public String getPriceDate(){
		return this.priceDate;
	}
	
	public String getTradingStatus(){
		return this.tradingStatus;
	}
	
	@Override
	public int hashCode(){
		return this.instrumentId.hashCode();
	}
	
	@Override
	public boolean equals(Object s){
		s = s;
		if(this.hashCode() == s.hashCode())
			return true;
		return false;
	}
	
	
}
