package com.novatia.lc.types;

public final class Side {
	
	public static final String buy = "B";
	public static final String sell = "S";
	
	public static String getSide(String side){
		if(side.equals(buy)){
			return "Buy";
		}else{
			return "Sell";
		}
	}

}
